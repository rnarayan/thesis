\chapter{The Standard Model of Particle Physics}\label{chap:standardmodel}
Our current understanding of particle physics is based on a theory called Standard Model~\cite{Glashow1961579}~\cite{PhysRevLett.19.1264} which explains various interactions observed in nature. In this chapter a brief review of the Standard Model is given.

%\section{The standard Model}

The Standard Model describes most of the known interactions in the universe. It describes the electromagnetic interactions which we directly experience through our senses, Weak interactions which are responsible for the decay of radioactive nuclei, It also describes the Strong interactions which are responsible for nuclear interactions. The Standard model evolved through a series of strides in experimental break throughs and theoretical insights. Even though the best known theory of gravitation, the general theory of relativity has not been successfully incorporated in the Standard Model, it has been fairly successful till date in describing particle intractions. 

Particles which obey Bose-Einstein statistics (bosons) and Fermi-Dirac statistics (Fermions), constitute the Standard Model. The integer spin bosons mediate particle interactions, while the spin $1/2$ fermions makes up the matter. The fermions in the Standard Model are classified into two categories, the quarks and the leptons. Quarks can exist only in bound states while the leptons can exist freely. The particles in the Standard Model are arranged in three generations. All the stable atoms and molecules that we see around are made up of the first generation particles in the Standard Model.

The fermions in the Standard Model interact with each other via exchanging force particles, the gauge bosons. Each interaction is mediated through the exchange of distinct gauge boson.

All electrically charged particles interact via the electromagnetic interaction by exchanging force particle, the  photon. The photon is a spin 1 massless boson with no electric charge. Since the photon is electrically neutral there are no self interactions and since it is massless the range of electromagnetic force is infinite. At the quantum level, the electromagnetic interaction is described by a theory called Quantum Electrodynamics. 


Quarks carry a so called ``color charge'' and interact via Strong interaction. Strong interactions are mediated by force carriers called gluons. The gluons themselves carry color charge and hence self interactions are possible. Even though the force particle is massless, the range of strong force is very limited due to reasons that will be explained in the following section. The strong force is the strongest of all known interactions. It is responsible for binding the quarks inside the atomic nuclei. 

The weak interaction is weaker than strong and electromagnetic interactions. It acts on all the particles in the Standard Model. The force carriers, the $W^\pm$ and the Z boson are massive and hence the range of the force is limited. The weak force is responsible for radioactive decay of atomic nuclei.


\section{Quarks and Leptons}

Quarks are the fundamental particles which undergoe strong interactions. In the standard model there are 6 of them arranged in 3 generations. The first generation consists of up and down quarks. The up quark has a fractional electric charge of +2/3e and the down quark has a charge of -1/3e (``e'' is the charge of the electron \SI{1.6021d-19}{\coulomb}). Similarly the other up type quarks, the top and charm have a charge +2/3e and the other down type quarks, strange and bottom have a charge -1/3e each.

The leptons are also organized into three families. There are six types of leptons electrons, muons, tauons and their respective neutrinos (electron neutrino, muon neutrino, tau neutrino). 

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{./images/chap2/sm_table.eps}
    \caption{Fundamental particles in the standard model. The spin quantum number, electric charge of the particles are listed with their masses~\cite{SM_table}.}
    \label{img:particle_table}
\end{figure}


The generation structure of quarks and leptons is shown in Figure~\ref{img:particle_table}. The first generation particles like up, down quarks and electrons form the ordinary matter. The higher generation particles are not stable and they decay into the first generation particles. All the particles listed in Figure~\ref{img:particle_table} also have their anti particles. These particles differ from their particle counterparts only in their quantum numbers. 

Quarks and anti-quarks do not exist free in nature and they only exist in bound states with another one or two quarks. Three quark bound states are called baryons. Protons (uud) and neutrons (ddu) are typical examples. Two quark bound states are called mesons. Particles like $\pi^+$ (u$\bar{\text{d}}$) and its anti particle $\pi^-$ ($\bar{\text{u}}$d) are mesons. 

In the following section we briefly review the mathematical formalism of Standard Model. The review is based on~\cite{halzen2008quark}.

\section{Gauge Theories}

In classical mechanics the dynamics of a system of interacting particles can be described using the Lagrangian of the system.  The Lagrangian L is defined as 
\begin{align}
    L & = T -V,
\end{align}

where $T$ is the kinetic energy and $V$ is the potential energy of the system.

The evolution of the system is described by the Euler-Lagrange equation
\begin{align}
    \frac{d}{dt}\left(\frac{\partial L}{\partial\dot{q_i}}\right) - \frac{\partial L}{\partial{q_i}} &=0,
\end{align}
where $\{q_i$, $\dot{q_i}\}$ are the generalized coordinates ($\dot{q_i} = dq_i/dt$). 
In quantum field theory, the discrete generalized co-ordinates are replaced with continuous systems, the fields $\phi\equiv\phi(\mathbf{x},t)$. The Lagrangian becomes
\begin{align}
    L\equiv L(\phi,\frac{\partial\phi}{\partial x_\mu},x_\mu),
\end{align}
where $\phi$ is the field.

It can be shown that the Dirac equation, the relativistic wave equation for a spin 1/2 particle, for instance the electron can be obtained by applying the following Lagrangian in the Euler-Lagrange equation. 
\begin{align}
    L & = i\psiup\gamma_\mu\partial^\mu\psiup - m\bar{\psiup}\psiup,
\end{align}
The electron is described by the complex field $\psiup(x)$. Clearly the Lagrangian is invariant under the phase transformation
\begin{align}
    \psiup(x)&\rightarrow e^{i\alphaup}\psiup(x),
\end{align}
Transformations of these kind are called gauge transformations since the ``gauge'' of the system is changed in the operation. The set of all transformations of the kind $U(\alphaup)\equiv e^{i\alphaup}$, where $\alphaup$ spans the set of all real numbers, forms a unitary Abelian group called U(1) group. By virtue of Noether's theorem it can be shown that such a gauge symmetry results in the conservation of electric charge. Thus the U(1) gauge symmetry of electromagnetic interactions conserves the electric charge. 

However under a ``local gauge transformation'' where $\alphaup$ depends on space-time co-ordinates,
\begin{align}
    \psiup(x)&\rightarrow e^{i\alphaup(x)}\psiup(x),
\end{align}
the Lagrangian is not an invariant. However nothing forbids the addition of extra terms to make the Lagrangian invariant. In case of the Lagrangian of Quantum Electrodynamics, these additional fields represent the photon field, the force carrier of the electromagnetic interaction.

Thus demanding a local gauge invariance generates the field particles of a given interaction.
\subsection{Strong Interaction}
Strong interaction is described by a gauge theory called quantum chromodynamics (QCD). The gauge group of the theory is $SU(3)_c$ (subscript 'c' denotes ``color''). Since not all generators of the group commute with each other the group is non Abelian. QCD describes the bound states of quarks mediated by the massless field particles, ie.\ through gluon coupling to color charges. Unlike in QED the gauge boson also carries a color charge and hence gluon self interactions are possible. Three types of color charges exist , ``red'', ``green'' and ``blue''. Eight gluons are needed to maintain the local gauge invariance under $SU(3)$.

The strength of the strong force is expressed through the coupling $\alphaup_s$. The coupling increases as the distance between the interacting particles increases and decreases as the distance decreases similar to how a glue acts. Therefore the field particles of the interaction which is responsible for holding the nucleons together is called ``gluon''. This would mean that only colorless particles are observed in nature, a phenomena called ``color confinement''. Color confinement restricts the range of strong interaction to \SI{d-15}{\meter} although the gauge boson of the interaction is massless.

%Color neutral QCD bound states exists as quark anti-quark pairs (mesons) or combinations of three quarks (baryons). The initial state of the process considered in this thesis involves protons. Protons are hadrons which consists of three valence quarks, two Up type and one Down type quark.

At small distances of interacting quarks, the QCD coupling becomes small, a phenomena known as asymptotic freedom. This behaviour allows for the perturbative calculation of interactions at high energy scales or small length scales. The cut off scale at which the perturbative regime of QCD stops is called $\lambdaup_\text{QCD}$ which is typically of the order of 200-300 MeV. Beyond this regime the quarks and gluons leave the quasi free regime to  QCD bound states generally called hadrons. Perturbative QCD is not applicable in describing such systems.  

\subsection{Electroweak Interaction}

The first hints of weak interaction came from the observation that lifetimes of pions and muons are longer than those particles which decay through electromagnetic or strong interactions. The Weak interactions are also responsible for the nuclear $\betaup$-decay. Fermi theory of $\betaup$-decay predicts the existence of a charge current responsible for the decay. This was experimentally verified through neutrino-electron scattering experiments. Later the Gargamelle experiment reported the existence of a weak neutral current~\cite{Hasert:1973ff} and this was also successfully incorporated into the theory.

The weak interaction is the only known interaction which can change the flavor of the quarks. It is also known to violate parity and the combined operation of charged conjugation and parity (CP). The gauge bosons of the theory are the heaviest compared to other known interactions, hence the range of the interaction is very small. 

The weak interaction couples to two quantum numbers, weak isospin ($I_W$) and hypercharge (Y). Hyper charge is given by the Gellmann-Nishijima formula 
\begin{align}
    Y &= 2(Q - I_{w3}),
\end{align}
where $I_{w3}$ is the third  component of weak isospin and Q is the electric charge of the particle. 

Left handed particles, that is particles which have negative chirality have $I_{w3}=1/2$. In general these particles can be grouped into weak isosopin doublets $I_{w3}=\pm1/2$. Right handed particles, which have positive helicity have $I_{w3}=0$ and forms a singlet state and do not undergo weak interactions. Local gauge invariance under the $SU(2)_L$ symmetry group rotates a particle in isospin space and transforms it into its doublet partner. 

In order to keep the weak Lagrangian invariant under $SU(2)_L$ three additional gauge fields are introduced in the Lagrangian, namely $\{W_1,W_2,W_3\}$. The physical field responsible for charge current interaction is a linear combination of the first two fields. 

\begin{align}
    W^{\pm} &= \frac{1}{\sqrt{2}}(W_\mu^1 \mp iW_\mu^2),
\end{align}

An additional $U(1)_Y$ gauge transformation is introduced to explain the weak neutral current interaction. Now the Lagrangian has to be invariant under the combined transformation $SU(2) \times U(1)$. This is achieved by the introduction of neutral field B which couples to the weak hypercharge Y. 

The physical fields that correspond to the neutral current gauge boson $Z^0$ and A, the photon of the electromagnetic field are given by the linear combination of the $W_3$ field and weak hypercharge field B. 
\begin{align}
    Z^0 &= W_3\cos\thetaup_W - B\sin\thetaup_W\\
    A  &= W_3\sin\thetaup_W + B\cos\thetaup_W,
\end{align}

the weak mixing angle $\thetaup_W$, or the Weinberg angle is a parameter that has to be determined experimentally.  

The weak eigenstates of quarks are not the same as the mass eigenstates. As a result the quark flavors can undergo mixing. An up quark can be converted to a down quark and otherwise. The flavor change is mediated through charge current interactions. Neutral current mediated flavor changes are not observed in nature. The transition probability between different quark flavors is described by the Cabibo-Kobayashi-Maskawa matrix (CKM-matrix).

\begin{align*}
    \left(\begin{array}{c}
            d^\prime \\
            s^\prime \\
            b^\prime\\
        \end{array}
    \right)
    & = 
    \left(\begin{array}{ccc}
            V_{ud} & V_{us} & V_{ub}\\
            V_{cd} & V_{cs} & V_{cb}\\
            V_{td} & V_{ts} & V_{tb}\\
        \end{array}
    \right)
    \left(\begin{array}{c}
            d\\
            s\\
            b\\
        \end{array}
    \right)
\end{align*}


\section{Electroweak Symmetry Breaking and Higgs Mechanism}

In the $SU(2)_L\times U(1)_Y$ gauge description of electro-weak interaction, all the gauge bosons need to be massless to preserve the gauge symmetry. However, this description is not true since $W$ and $Z$ bosons are indeed observed to be massive. The introduction of explicit mass terms in the Lagrangian causes divergences in the theory. The problem is solved by introducing an isosopin doublet with weak hypercharge Y=1.
\begin{align}
    \begin{aligned}
        \phi &= 
        \left(\begin{array}{c}
                \phi^{+}\\
                \phi^0
            \end{array}
        \right)
    \end{aligned}
    \begin{aligned}
        \quad \text{with} \quad
        \phi^+ &= (\phi_1 + i\phi_2)/\sqrt{2}\\
        \phi^0 &= (\phi_3 + i\phi_4)/\sqrt{2},
    \end{aligned}
\end{align}
with the choice of a covariant derivative 
\begin{align}
    D_\mu &=\partial_\mu + ig\frac{\tauup_a}{2}W_\mu^a,
\end{align}
where $W_\mu^a$'s are the three gauge fields,$\tauup_a$'s are the generators of $SU(2)$ group and $g$ is the coupling constant, the following Lagrangian is guaranteed to be invariant under SU(2) local gauge transformations.

\begin{align}
    L &= (D_\mu\phi)^\dagger(D^\mu\phi) -V(\phi) -\frac{1}{4}W_{\mu\nu}W^{\mu\nu},
\end{align}
where $V(\phi)$ is the potential given by 
\begin{align*}
    V(\phi) &= \mu^2\phi^\dagger\phi + \lambdaup(\phi^\dagger\phi)^2\\
\end{align*}

For $\lambdaup>0$ and $\mu^2 <0$, the potential has a minimum at 
\begin{align}
    \left|\phi\right| = -\frac{\mu^2}{2\lambdaup}
\end{align}

This set of all points at which $V(\phi$) is minimum is invariant under $SU(2)$ transformations. The potential is minimum with the following choice of components. 
\begin{align}
    \begin{aligned}
        \phi_1 = \phi_2 = \phi_4 = 0
    \end{aligned}
    \quad,\quad
    \begin{aligned}
        \phi_3^2 &= -\frac{\mu^2}{\lambdaup}\equiv v^2
    \end{aligned}
\end{align}
The Lagrangian description of the system is not expected to change by choosing a particular value of the field, in this case the ground state. However it is observed that the Lagrangian looses reflection symmetry when the field is expanded around the stable vacuum. Thus the symmetry is ``spontaneously broken''. The ground state of the field $\phi(x)$ or the ``vacuum expectation value'' is given by
\begin{align}
    \phi_0 &=\sqrt{\frac{1}{2}}
    \left(
        \begin{array}{c}
            0\\
            v
        \end{array}
    \right)
\end{align}

Excitations from the ground state gives rise to the physical Higgs field
\begin{align*}
    \phi(x) = \sqrt{\frac{1}{2}}
    \left(
        \begin{array}{c}
            0 \\
            v + h(x)
        \end{array}
    \right)
\end{align*}

Out of the four scalar fields introduced the Higgs field h(x) is the only remaining one.

The gauge boson masses generated can be determined by substituting the vacuum expectation value into the Lagrangian. The term that we are interested in is $(ig/2\tauup W_\mu\phi)^\dagger(ig/2\tauup W^\mu\phi$).

\begin{align*}
    \left|ig\frac{1}{2}\tauup W_\mu\phi\right|^2 &=\frac{g^2}{8}
    \left|
    \left(
        \begin{array}{cc}
            W_\mu^3 & W_\mu^1 - iW_\mu^2\\
            W_\mu^1 + i W_\mu^2 & W_\mu^3
        \end{array}
    \right)
    \left(
        \begin{array}{c}
            0\\
            v
        \end{array}
    \right)
    \right|^2\\
    &=\frac{g^2v^2}{8}\left[(W_\mu^1)^2 + (W_\mu^2)^2 + (W_\mu^3)^2\right],\\
\end{align*}

However $(W_\mu^1)^2 + (W_\mu^2)^2$ is the product of physical fields $2W_\mu^+W_\mu^-$. Therefore 

\begin{align*}
    \left|ig\frac{1}{2}\tauup W_\mu\phi\right|^2 &= \left(\frac{1}{2}vg\right)^2W_\mu^+W_\mu^- + \frac{g^2}{8}(W_\mu^3)^2
\end{align*}

Comparing the first term with the mass term expected for a boson we have 
\begin{align*}
    M_W = \frac{1}{2}vg
\end{align*}

Similarly the photon mass (which is zero) and Z-boson mass can be deduced from an $SU(2)_L\times U(1)_Y$ gauge invariant Lagrangian. The mass of the Z-boson can be shown to be 
\begin{align*}
    M_Z = \frac{1}{2}v\sqrt{g^2 + {g^\prime}^2}
\end{align*}

where $g^\prime$ is the strength of the coupling to $U(1)_Y$ group. 

The fermions acquires mass through Yukawa coupling to the Higgs field. For example, to generate the electron mass an $SU(2)\times U(1)$ gauge invariant term is introduced to the Lagrangian 
\begin{align}
    L^\prime  &= -\lambdaup_e
    \left[(\bar{\nuup_e},\bar{e})_L
        \left(
            \begin{array}{c}
                \phi^+\\
                \phi^0
            \end{array}
        \right)e_R + 
        \bar{e}_R(\phi^-,\bar{\phi^0})
        \left(
            \begin{array}{c}
                \nu_e\\
                e
            \end{array}
        \right)_L
    \right]
\end{align}

After spontaneously breaking the symmetry
\begin{align*}
    \phiup &= \sqrt{\frac{1}{2}}
    \left(
        \begin{array}{c}
            0\\
            \nu+h(x)
        \end{array}
    \right)
\end{align*}

Therefore 
\begin{align*}
    L^\prime &=-\frac{\lambdaup_e}{\sqrt{2}}(\bar{e_L}e_R + \bar{e_R}e_L)\nu - \frac{\lambdaup_e}{\sqrt{2}}(\bar{e_L}e_R + \bar{e_R}e_L)h
\end{align*}

The standard mass term of a fermion is given by 
\begin{align*}
    m\bar{\Psi}\Psi &=m\bar{\Psi_L}\Psi_R + m\bar{\Psi_R}\Psi_L
\end{align*}

Comparing the above two expressions we identify the electron mass to be,
\begin{align}
    m_e &=\frac{\lambdaup_e\nu}{\sqrt{2}}
\end{align}

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap2/mex_hat.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap2/higgsTogamma_gamma.eps}
        \caption{}
        \label{img:higgs-diphoton}
    \end{subfigure}
    \caption{(a) The Higgs potential for $\mu<0$, $\lambdaup>0$, Higgs-boson mass of \SI{126.8}{\giga\electronvolt} and a vacuum expectation value of \SI{246}{\giga\electronvolt}. Adapted from~\cite{mexhat_siya} (b)The Higgs signal observed in the diphoton channel. The fit is consistent with a Higgs hypothesis with mass \SI{126.8}{\giga\electronvolt}~\cite{tagkey201388}}
\end{figure}


The ATLAS and CMS collaborations at the LHC have detected a new particle which is compatible with the Standard Model Higgs boson~\cite{Aad20121}~\cite{Chatrchyan201230}. All properties of this new particle measured until now are within the expectation of the Standard Model Higgs boson. In Figure~\ref{img:higgs-diphoton}, the resonance peak of the Higgs boson decaying into two photons is shown. The measured mass of the Higgs boson has been found to be \SI[parse-numbers=false]{125.36\pm0.37(stat)\pm0.18(syst)}{\giga\electronvolt}~\cite{Aad:2014aba}.

\section{Theories Beyond Standard Model}
\begin{figure}[t]
    \centering
    %\includegraphics[width=0.45\textwidth]{./images/chap2/gfitter_smfits.eps}
    %\caption{Deviations of standard model fits to data from theoretical predictions normalized to the measurement uncertainty}
    \includegraphics[width=\textwidth]{./images/chap2/ATLAS_c_SMSummary_TotalXsect_rotated.eps}
    \caption{Standard Model cross sections predicted by theory and measured by the ATLAS experiment. The plot shows that the measured cross sections, within the uncertainties do not deviate significantly from the predictions~\cite{ATLAS_sm_publicResults}.}
    \label{img:gfitter_plot}
\end{figure}
Standard Model is an extremely successful theory. The agreement between Standard Model predictions and measurements is shown in Figure~\ref{img:gfitter_plot}.  However, the Standard Model is not a complete theory. There are problems in the theory. 

In the Standard Model, the neutrinos are massless, however neutrino oscillations, a phenomenon in which the flavor eigenstates evolve in time into another flavor eigenstate has been observed. This suggests a non-zero neutrino mass. %Standard Model is not able to explain the large difference in coupling constants of forces at different energy scales, This is an open problem known as the Hierarchy problem. 

So far gravity has not been included in Standard Model. Attempts to develop a renormalizable quantum field theory of gravity were all unsuccessful. The Standard Model is also not able to predict the baryonic asymmetry in the universe. The universe is primarily made up of matter particles compared to anti-matter particles. The Standard Model offers no explanations for this asymmetry. 

Cosmological models shows that our universe consists of only 4\% matter that is made up of Standard Model particles. The remaining is thought to be 27\% dark matter and 69\% dark energy or \textit{cosmological constant}. Quantum field theories predict the existence of zero-point energy or vacuum energy of space. It also observed in phenomena like Casimir effect. The vacuum energy should contribute to the \textit{cosmological constant} term in the Einstein field equations of general relativity. However the measured value of cosmological constant differ significantly from the vacuum energy measurements by a staggering 120 orders of magnitude~\cite{lambda_cosm_val}.

Galaxy rotation curves confirm the presence of dark matter aggregates which only interact gravitationally with ordinary matter. However the Standard Model offers no candidates for this kind of elusive particles which probably only interact very weakly with ordinary matter.

Another problem is the \textit{hierarchy problem}. Loop corrections to the Higgs boson mass is given by 
\begin{align}
    \Delta m_H^2 &= -\frac{\left|\lambdaup_f\right|^2}{8\pi^2}\Lambda_\text{UV}^2 +\ldots
\end{align}
where $\lambdaup_f$ is the Yukawa coupling of a fermion $f$ and $\Lambda_\text{UV}$ is the maximum energy scale up to which the theory is valid. This is usually considered to be the Planck Scale (\SI{d19}{\giga\electronvolt}). It is clear that loop corrections to Higgs boson mass are extremely large contrary to the observed higgs mass that is at electro-weak scale. There is no solution for this problem in the Standard Model

One of the prominent beyond standard model theory is based on a symmetry called ``supersymmetry''. According to this theory, each fermion in the standard model has a corresponding bosonic super partner and each boson has a corresponding fermionic super partner. Except for the spins, all other quantum numbers are the same. Since no super parners of comparable masses has been observed, the symmetry is not an exact symmetry. If it exist, it must be a broken symmetry. Super symmetry naturally solves the hierarchy problem as the bosonic and fermionic contributions to Higgs boson mass cancel each other.

Since the energy scales probed by the Large Hadron Collider are the highest until now, supersymmetric models can be tested even beyond the electroweak scale. In many super symmetric models the lightest super symmetric particle is stable and neutral and interacts very weakly with ordinary matter, making it an ideal candidate for Dark Matter.

Searches for several super symmetric models have been carried out at the LHC at \SI{7}{\tera\electronvolt} and \SI{8}{\tera\electronvolt} center of mass collision energies. However, all the results have been negative so far.

%\bibliographystyle{atlasBibStyleWithTitle}
%\bibliography{chap2_standardmodel.bib}


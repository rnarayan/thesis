\chapter{Electron Reconstruction}\label{chap:stdElectron}

Electrons are primary signatures of electro-weak processes. They are crucial in precision tests of the Standard Model at the LHC. High $\pt$ electrons are signatures of several New Physics searches. An efficient and precise electron identification was one of the main goals of the ATLAS detector design. In this chapter the electron reconstruction and various standard ATLAS identification techniques are reviewed. Throughout this thesis unless otherwise specified electron means both electron and positron. 

\section{Electron Reconstruction}

\begin{figure}
    \centering
    \includegraphics[scale=0.3]{./images/chap7/electron_cand_atlantis.eps}
    \caption{An event display of an electron plus jet candidate. The downward pointing orange track with an associated green cluster is the electron candidate.}
    \label{img:electron_atlantis}
\end{figure}

An electron is characterized by a track in the Inner Detector (ID) associated with a narrow cluster of energy deposition in the electromagnetic calorimeter. An event display of lepton + jet event is shown in Figure~\ref{img:electron_atlantis}. The track reconstructed in the ID points to a very narrow cluster of energy deposition in the electromagnetic calorimeter.  There is no activity in the hadronic calorimeter cells behind the clusters in the electromagnetic calorimeter

Electron candidates are reconstructed by associating tracks in the ID with local energy depositions in the electromagnetic calorimeter using a sliding window clustering algorithm~\cite{Lampl:2008zz}. Electron interactions in the calorimeter produce particle showers, which ultimately deposit energy in the calorimeter cells. The algorithm scans the entire $\eta$-$\phi$ space and builds clusters from the cells. The clustering is seeded by a ``hot cell'' in a small search window of 3 units in  $\eta$ and 5 units in $\phi$ (1 unit = 0.025 in $\eta$ and $\phi$, which is the cell granularity of the middle layer of the electromagnetic calorimeter). These clusters are required to have a minimum transverse energy of 2.5 GeV. 

In the next step, the ID tracks are associated to the clusters. Tracks are extrapolated from the Inner Detector to the second layer of the electromagnetic calorimeter and the differences between $\eta$ and $\phiup$ coordinates of the cluster and track is calculated. This track--cluster matched candidate is considered to be an electron candidate if 
\begin{itemize}
    \item $\Delta\eta_\text{calo-track} < $ 0.05,
    \item $\Delta\phi_\text{calo-track} < $ 0.1,
\end{itemize}

where $\Delta\eta_\text{calo-track}$ and $\Delta\phi_\text{calo-track}$ are the $\eta$ and $\phi$ difference between the respective coordinates of the clusters bary-center and the extrapolated track.

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/reco_eff_eta.eps}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/reco_eff_Et.eps}
    \end{subfigure}
    \caption{Electron reconstruction efficiency as a function of  (a) cluster $\eta$ and (b) cluster $E_{\text{T}}$. The red curve represent 2011 data and the blue curve represent 2012 data~\cite{ATLAS-CONF-2014-032}.}
    \label{el_eff}
\end{figure}

Particles which lose energy due to bremstrahlung in the inner detector bend stronger in $\phi$ than naively expected. Therefore the cut in $\phi$ is loosened to $0.1$ to take care of these electron candidates. If more than one track satisfy these matching criteria, then the one which has the lower $\Delta R$ value is chosen.

The total material in the active region of the inner detector $|\eta| < 2.5$ is roughly 50\% of X$_0$. This means that the electron could potentially lose a significant fraction of its energy due to bremsstrahlung before reaching the calorimeter. However these radiated photons reaching the calorimeter are contained in the calorimeter volume closely associated to the electron clusters. This makes it possible to recover some of the bremstrahlung losses in case the electron is not undergoing hard bremsstrahlung. For the data taking period of 2012, an additional track reconstruction algorithm was used which accounts for these bremstrahlung losses. Tracks associated with the seed clusters are refitted using a Gaussian Sum Filter (GSF) algorithm~\cite{ATLAS-CONF-2012-047}.

After the track matching, the seed clusters are rebuilt, the cluster size is enlarged to $3\times7$ in $\eta-\phi$ in the central barrel region and $5\times5$ in the end-cap region. The total energy of the electron is determined from 4 components~\cite{Aad:2011mk}:
\begin{itemize}
    \item The energy measured in the cluster. 
    \item The estimated energy loss before the electron reaches the calorimeter.
    \item The estimated energy loss laterally outside the calorimeter.
    \item The estimated energy loss leaked in the longitudinal direction in the calorimeter.
\end{itemize}
The electron candidates at this stage of reconstruction are referred to as ``container electrons''. The efficiency to find electron candidates with this method is shown in Figure~\ref{el_eff}. The use of GSF algorithms has significantly improved the electron candidate reconstruction efficiency.

Apart from prompt electrons, reconstructed electron candidates also consist of hadrons, conversion electrons, electrons from $\pi^0$ decays and semileptonic decays of heavy flavor particles. In the following electron identification for prompt isolated electrons is described. Development of a method for identifying electrons from heavy-flavor decays is described in chapter~\ref{chap:softe}

\section{Electron Trigger}
Isolated electrons like those coming from Z-boson decays provide a clean trigger signals in the calorimeter. At L1, electron candidates are selected by requiring electromagnetic trigger towers to exceed an $\et$ threshold. In order to reduce hadronic fakes a hadronic veto is also applied to L1 electron triggers. True electrons are expected to deposit only a small amount of energy in the hadronic calorimeter behind the electron cluster. The L1 electron trigger defines a region of interest which acts as a seed for electron reconstruction in HLT. Offline electron reconstruction algorithms are available at this level to identify electrons. 

Two categories of electron triggers exist, primary trigger and supporting trigger. Primary triggers are un-prescaled triggers which employ strict particle identification criteria. They are used in physics analysis. The supporting triggers select electrons based on the $\et$ cuts without any identification criteria. Thus they offer a very unbiased electron candidate sample which can be used for performance studies. Since they do not use any identification criteria, the rates are high and hence supporting triggers are heavily prescaled. 

\section{Electron Variables}
Prompt decays of W and Z bosons produce largely isolated electrons. However, electrons coming from the decay of heavy flavor particles like B-hadrons and D-mesons are not so well isolated, because they are produced in association with jets. There is no inclusive way of identifying both the isolated and non-isolated components. Most of the effort in ATLAS is concentrated on optimizing the identification of isolated prompt electrons for searches or identifying vector bosons

The separation between real electrons and background can be achieved by using variables measured in the calorimeter as well as in the Inner Detector that can be used to identify electrons.

\subsection{Calorimeter Variables}

\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Rhad1.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Rhad.eps}
        \caption{}
    \end{subfigure}
    \caption{Energy leakage into the hadronic calorimeter parameterized as (a) $R_\text{had1}$ : Ratio of energy deposited in the first layer of the hadronic calorimeter to the energy deposited in the electron cluster (b)$R_\text{had}$ : Ratio of energy deposited in all hadronic layers to the energy deposited in the electron cluster.}
    \label{img:rhad}
\end{figure}


The electromagnetic calorimeter has three layers with fine segmentation in $\eta$ and $\phi$. The response of the electromagnetic calorimeter to incident particles is parameterised using ``shower shape variables''.

Electromagnetic calorimeter activity related to the electrons are predominantly contained within its volume without any significant leakage into the hadronic calorimeter behind the electron cluster. If there is more activity in the hadronic calorimeter cells directly behind the electron cluster the candidate is less likely to be an electron.

The variable $R_\text{had1}$ is defined as the ratio of the energy deposited in the first layer of the hadronic calorimeter behind the electron cluster to the energy deposited in the electron candidate cluster, which can separate true electrons from hadrons. $R_\text{had1}$ distributions for different categories of electron candidates are shown in Figure~\ref{img:rhad}. Large values of $R_\text{had1}$ indicates hadronic activity associated to the cluster. In the transition region between the barrel and end-cap of the hadronic calorimeter $(0.8<|\eta|<1.37)$, the hadronic leakage is calculated using all layers of the hadronic calorimeter denoted by a new variable $R_\text{had}$. 


Since the electron showers are more likely to be contained within the volume of the electromagnetic calorimeter, the electromagnetic shower develops in the first sampling layer, increases the shower activity in the second layer and dies out in the third sampling layer. Thus true electrons deposit usually less energy in the third sampling layer. The ratio of energy deposited in the third sampling layer to the total energy of the electron cluster could be another potential discriminating variable to separate electrons from hadrons. Two different parameterizations are available for this
\begin{align*}
    \begin{aligned}
        f_3  & = \frac{E_3}{E},\\
    \end{aligned}
    \quad\quad \text{and} \quad\quad
    \begin{aligned}
        f_3(\text{core}) &= \frac{E_3(\text{core})}{E},\\
    \end{aligned}
\end{align*}


\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/F3.eps}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/F3core.eps}
    \end{subfigure}
    \caption{Discriminating variables sensitive to the electron energy deposition in the third layer of the electromagnetic calorimeter.}
    \label{img:f3}
\end{figure}

where $E_3$ is the cluster energy deposited in the third sampling layer and $E_3(\text{core})$ is the energy deposited in the core region (corresponds to $3\times3$ cells in $\eta-\phi$) of the calorimeter cluster. In regions with lot of activity the $E_3\text{(core)}$ variables are expected to show better discrimination. The distributions are shown in Figure~\ref{img:f3}. As expected at higher $f_3(\text{core})$ values hadron contributions are dominant compared to electrons. Although non isolated electrons have low energy compared to prompt electrons, their $f_3(\text{core})$ values tend to be harder compared to the prompt electrons. This is due to the high calorimeter activity around a non isolated electron.

The shower width in the second sampling layer can be parameterised in three independent variables. $w_{\eta2}$ shown in Figure~\ref{img:weta2}, measures the width as energy weighted root mean square of the $\eta$ distribution in the cells of the second sampling layer.  It is defined as
\begin{align*}
    w_{\eta^2} &= \sqrt{\frac{\sum_iE_i\eta_i^2}{\sum_iE_i} - \left(\frac{\sum_iE_i\eta_i}{\sum_iE_i}\right)},
\end{align*}
where the index $i$ runs over the $3\times5$ cells window of the second sampling layer, around the electron cluster bari-center. 
\begin{figure}
    \centering
    \includegraphics[scale=0.6]{./images/chap7/self_made/Weta2.eps}
    \caption{The shower width measured as the RMS of the $\eta$ distribution of the cells in the second sampling layer. Isolated electrons have a narrow profile.}
    \label{img:weta2}
\end{figure}

$R_\eta$ and $R_\phi$ are the other two variables which measure the shower width in the second sampling layer. $R_\eta$ is defined as the ratio of energy deposited in the $3\times7$ cells in $\eta-\phi$ to the energy deposited in $7\times7$ cells in $\eta-\phi$ in the second layer of the electromagnetic calorimeter. Similarly $R_\phi$ is defined as the ratio of energy deposited in the $3\times3$ cells in $\eta-\phi$ cells in $\eta-\phi$ to the energy deposited in the $3\times7$ cells in $\eta-\phi$ in $\eta-\phi$ in the second sampling layer.

\begin{equation*}
\begin{split}
    R_\eta & = \frac{E_{(3\times7)}}{E_{(7\times7)}}\\
\end{split}
\quad\quad\text{and}\quad\quad
\begin{split}
    R_\phi & = \frac{E_{(3\times3)}}{E_{(3\times7)}},\\
\end{split}
\end{equation*}

where $E_{(3\times7)}$ is the energy deposited in $3\times7$ cells in $\eta-\phi$ of and $E_{(7\times7)}$ is the energy deposition in $7\times7$ cells in $\eta-\phi$ of the second sampling layer. A schematic representation as well as variable distribution is shown in Figure~\ref{img:ratio}.

\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/reta_scheme.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/rphi_scheme.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Reta.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Rphi.eps}
        \caption{}
    \end{subfigure}
    \caption{Schematic representation of (a) $R_\eta$ and (b) $R_\phi$. Both variables represents ratio energies deposited in a group of calorimeter cells. Energy deposited in yellow regions are numerators and green regions are the denominators in the ratios. The distributions for respective variables are shown in (c) and (d). Isolated electrons have a narrow profile in both variables.}
    \label{img:ratio}
\end{figure}

The first layer of the electromagnetic calorimeter has the highest granularity among all three layers. Therefore the lateral development of the shower can be reconstructed more precisely there.

The variable $E_\text{ratio}$, defined as
\begin{align*}
    E_{ratio} &= \frac{E^s_{1^{st}max} - E^s_{2^{nd}max}}{E^s_{1^{st}max} + E^s_{2^{nd}max}},
\end{align*}
where $E^s_{1^{st}max}$ is the first maximum and  $E^s_{2^{nd}max}$ is the second maximum energy deposition in the strip layer. This variable is sensitive to the difference of the first and second maximum energy deposition in the strip layer. Objects like jets have multiple particles associated to the same cluster. The energy deposited by these particles in the strip layer tend to be more or less equal, hence a difference in first and second maximum energy deposition would be less. However this is not expected for a true electron as it can be seen in Figure~\ref{img:eratio}. 

The width of the shower in the first sampling layer or the strip layer is another variable which can separate electrons from background. The variable is parameterised as 
\begin{align}
    w_{s,tot} &= \sqrt{\frac{\sum_iE_i(i - i_{max})^2}{\sum_iE_i}}
\end{align}
The distribution is show in Figure~\ref{img:wstot}

where $E_i$ is the energy in the $i^{th}$ strip, $i$ is the strip index, and $i^{max}$ is the index of the strip with the most energy. The sums runs over $20\times2$ strips in $\eta\times\phi$ around the cluster center. 


\begin{figure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Eratio.eps}
        \caption{}
        \label{img:eratio}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Wstot.eps}
        \caption{}
        \label{img:wstot}
    \end{subfigure}
    \caption{(a) $E_\text{ratio}$ and (b) $w_\text{s,tot}$ shower shape variables in the first layer of electromagnetic calorimeter or the first layer.}
\end{figure}

Low energy electrons doesn't have much shower activity as high energy electrons. Hence they lose energies predominantly in the first layer of the electromagnetic calorimeter. Two variables which exploit this feature are shown in Figure~\ref{img:f1}. The variables are defined as the following.
\begin{align*}
    \begin{aligned}
        f_1  & = \frac{E_3}{E},\\
    \end{aligned}
    \quad\quad \text{and} \quad\quad
    \begin{aligned}
        f_1(\text{core}) &= \frac{E_3(\text{core})}{E},\\
    \end{aligned}
\end{align*}

where $E_3$ is the cluster energy deposited in the third sampling layer and $E(\text{core})_3$ is the energy deposited in the core region (corresponds to $3\times3$ cells in $\eta-\phi$) of the calorimeter cluster.

\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/F1.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/F1core.eps}
        \caption{}
    \end{subfigure}
    \caption{(a) $f_1$ and (b) $f_1(\text{core})$ distributions. The ratio of energy deposition in the first layer to the total cluster energy and the ratio of energy deposition in the core region of the cluster in the first layer to the total cluster energy respectively.}
    \label{img:f1}
\end{figure}

\subsection{Inner Detector Variables}
One of the main background to real electrons is the photon conversion background. The cross section for pair production is high while energetic photons interact with matter. Since the distribution of material increases upstream and the pixel and strip detectors are thin silicon technologies, the pair production probability is much smaller in the inner detector. Therefore pixel and strip layer hit requirements for electron candidate tracks reduce the conversion background greatly without compromising the signal efficiency. Of particular interest is the hits in the first pixel layer or b-layer. The only material before this layer is the beam pipe. A b-layer hit requirement is the most sensitive variable against photon conversions. The hit distributions are shown in Figure~\ref{img:idhits}.

\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/NBLHits.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        %\includegraphics[width=\textwidth]{./images/chap7/self_made/NPixHits.eps}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/NSiHits.eps}
        \caption{}
    \end{subfigure}
    \caption{(a) number of b-layer hits and (b) number of hits in pixel layer and SCT in total.}
    \label{img:idhits}
\end{figure}

The transverse impact parameter distribution $d_0$ (Figure~\ref{img:d0}) is a measure of the distance of the closest approach of the electron track to the primary vertex. Prompt electrons tends to have a smaller value for $d_0$ than conversion electrons which are produced in the material upstream. Since long lived B-hadrons have a displaced secondary vertex heavy-flavor decay electrons also have large impact parameter values. However, it tends to be smaller than that of conversion electrons.

\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/Trkd0physics.eps}
        \caption{}
        \label{img:d0}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/ConversionBit.eps}
        \caption{}
        \label{img:convBit}
    \end{subfigure}
    \caption{(a) Transverse impact parameter, $d_0$ distribution and (b) Conversion bit distribution. 0 indicates no conversions and 2 indicates 2 leg conversions.}
\end{figure}


A conversion finder algorithm is used to find the electron candidates originating from the photon conversions in the ID. To each electron candidate the algorithm assigns conversion bits one out of possible three. If the algorithm failed to identify the electron candidate as a conversion electron, then a bit 0 is set. Photon conversions produce pair of opposite charged electrons. However, sometimes one of the electrons could be outside the detector acceptance, leaving the other lone pair within the active region of the detector. To account for these types of conversion electrons the algorithm flags all candidates which doesn't have a b-layer hit as single leg conversion electron and a conversion bit value of 1 is set. In case of double leg conversions where the two conversion electron candidates propagate through the active detector volume, all kinematic properties of photon conversions are used. The two electron candidates are required to be oppositely charged, with a small opening angle between their tracks. Since conversion photons have a common vertex, vertex of the di-electron system is required to match with the material geometry in the ID. Figure~\ref{img:convBit} shows conversion bit distribution of various electron candidates.  A conversion bit requirement of 0, ie.\ no conversion, removes a significant fraction of conversion background without compromising much on the efficiency for non converted electrons. 

\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/NTRTHtHits.eps}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/TRTHtHitRatio.eps}
    \end{subfigure}
    \caption{(a) Number of TRT hits and (b) TRT high threshold hit fraction. Being heavier hadrons doesn't produce transition radiation photons as much as electrons.}
    \label{img:trt_fraction}
\end{figure}


When a charged particles passes through the Transition Radiation Tracker (TRT), transition radiation photons are produced. The transition radiation produced is proportional to the Lorentz $\gamma$ factor ($\gamma=E/m$). For a given energy massive particles have a low gamma factor. Thus massive charged particles like pions and other hadrons tend to produce less transition  radiation. The transition radiation photons ionize the gas. The ionization charges are collected and registered as a hit. The hits are classified as high threshold hits and low threshold hits corresponding to \SI{6}{\kilo\electronvolt} and \SI{300}{\electronvolt} transition radiation photon energies respectively~\cite{abat2008atlas}. The distribution of number of TRT hits and TRT high threshold hit fraction is shown in Figure~\ref{img:trt_fraction}. For hadrons, the fraction of high threshold TRT hits peak at lower values in contrast to electrons. Therefore this fraction can be used as a good discriminant to separate electrons from hadrons. 
\section{Combination of Track and Cluster Variables}
As it can be seen from the distributions, the calorimeter variables are sensitive to $\et$ and $\eta$. The shower shape development is a function of $\et$, high energy particles tend to have narrower shower profiles. Isolated electron identification tends to improve as one goes high in $\et$. The $\eta$ dependence is mainly due to the projective geometry of the calorimeter. The region $1.37 <|\eta|< 1.52$ is called crack region where a transition between barrel calorimeter to end-cap calorimeter happens. Since the background rejection in this region is very poor due to the presence of additional material, the region is excluded from physics analysis. 


The inner detector variables are however largely independent of electron $\eta$ and $\et$ except for the TRT. The TRT response is dependant on $\eta$ because of the different material used in the barrel and end-caps. Since the readout frames of the ID are in nano seconds range it is immune to pileup. 

Combination of ID and calorimeter provides additional discrimination power. Tracks reconstructed in the ID can be extrapolated to the calorimeter and compared to the cluster position. $\Delta\eta$, the difference between the track and cluster eta tends to be less for real electrons. Other charged particles that can be produced in association with hadrons and conversion electrons can cause a shift in the cluster $\eta$ position. 

$\Delta\phi$ reflects the track cluster matching in $\phi$. Bremsstrahlung losses reduces the energy of the charge particle. In the solenoidal field these tracks curve more than expected, hence the $\Delta\phi$ distribution would be more broader than $\Delta\eta$ distribution. Both distributions are shown in Figure~\ref{img:delta_eta_phi}. The distributions are broader for both hadrons and conversion electrons. In case of hadrons this happens due to calorimeter activity in the neighboring cells. The conversion electrons are produced through photon interactions in the ID material. The average ID hit per conversion electron is less compared to real electrons (Figure~\ref{img:idhits}). The broad tails of conversion electrons in $\Delta\eta_2$ and $\Delta\phi_2$ distributions arise due to the imprecise track measurement in the ID.



\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/DeltaEta2.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/DeltaPhi2.eps}
        \caption{}
    \end{subfigure}
    \caption{Inner Detector tracks are extrapolated to the calorimeter. The distribution (a) shows the offset of $\eta$ measured in the tracker and the cluster $\phi$ measure in the strip layer of the calorimeter. Distribution (b) shows the offset in tracker $\phi$ and cluster $\phi$ reconstructed in the second layer of the calorimeter.}
    \label{img:delta_eta_phi}
\end{figure}


Another variable that can be used is the ratio of the energy reconstructed in the electromagnetic calorimeter to the momentum measured by the ID. In the ideal case, in the absence of bremstrahlung the energy reconstructed by the tracker and the calorimeter should be the same and the ratio should be 1. But in reality, due to the presence of ID and the electron energy is not fully reconstructed by the ID. The hadrons do not deposit their energy fully in the electromagnetic calorimeter. In the E/P distribution (Figure~\ref{img:eoverp}) hadrons tend to peak at low values, real electrons peak at 1 with a long tail due to bremstrahlung losses. Conversion electrons are emitted collinear hence both electrons happen to end up in the same cluster, giving a large value for E/P. 

\begin{figure}[t!]
    \centering
    \includegraphics[width=0.7\textwidth]{./images/chap7/self_made/EOverP.eps}
    \caption{The ratio of energy reconstructed in the calorimeter to the momentum measured in the ID. Real electrons show a peak at 1 since the energy deposited in calorimeter and the momentum reconstructed in the ID are comparable. The long tails are due to bremsstrahlung losses.}
    \label{img:eoverp}
\end{figure}

Another class of variables called isolation variables can be used to discriminate between signal and background. Isolation is a measure of the activity of energetic particles around the electron candidate. Isolation is calculated by adding energy in a cone centered around the electron. The cone size can be 0.2, 0.3 or 0.4 units in $\eta$-$\phi$ space. There are two classes of isolation variables. The one which is calculated with the energy deposited in the calorimeter and the other which is  calculated with the energy measured in the inner detector. Figure~\ref{img:isolation_var} shows $\et^\text{cone}(0.3)/\et$ and $\pt^\text{cone}(0.3)/\pt$  calorimeter and tracker isolation variables respectively. Hadrons and heavy-flavor electrons are produced in association with the other particles, hence they tend to have larger value of isolation compared to prompt electrons. 
\begin{figure}[t!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/EtCone30Iso.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap7/self_made/PtCone30Iso.eps}
        \caption{}
    \end{subfigure}
\caption{(a) Calorimeter isolation $\et^\text{cone}(0.3)/\et$ and (b) Tracker isolation variable $\pt^\text{cone}(0.3)/\pt$ showing separation between electrons and hadrons.}
    \label{img:isolation_var}
\end{figure}


Since the calorimeter measures the energy of neutral particles as well, calorimeter based isolation variables are more sensitive to particle activity than tracker based isolation. However the tracker based isolation is less sensitive to pileup effects. Hence the tracker and calorimeter based isolation variables are more or less complimentary. 

\section{Standard Electron Operating points}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{./images/chap7/electron_efficiency_nvtx.eps}
    \caption{Measured reconstruction efficiency for various electron selection menus, as a function of number of primary vertices. The menus are relatively stable to pileup ~\cite{ATLAS-CONF-2014-032}.}
    \label{img:isEm_pileup_stable}
\end{figure}

Many physics analysis which are looking for electrons has a prompt, isolated high $\pt$ electrons in the final state. A set of cuts are applied on the electron discrimination variables in order to achieve a sufficient purity and efficiency. These cut values are standardised in ATLAS which allows for the central handling of efficiency and systematics determination. 

In order to satisfy a broad range of physics requirements three menus of electron operating points are defined namely \textit{Loose}, \textit{Medium} and \textit{Tight} in the increasing order of electron purity. 

Among the discriminating variables described above, the isolation variables are not used in the menu. The isolation variables uses a large detector region (up to 0.4 in R). This requirement many not be universally optimal for all physics analysis. Depending on the physics case, individual analysis may select an optimal isolation cut. 

The cut values are tightened more and more in each menu. The menu was optimized using background PDFs obtained from data. Signal PDFs were obtained from Monte Carlo and data driven corrections were applied. The cut values in the cut menu were determined by the trigger requirements. At an instantaneous luminosity of \SI{d33}{\per\square\cm\per\second}, the \textit{Medium} operating point would correspond to a single electron trigger rate of \SI{20}{\hertz} for 20 GeV electrons. The \textit{Loose} and \textit{Tight} menu were set with respect to the \textit{Medium} operating point. The \textit{Loose} menu is obtained by relaxing the cuts in \textit{Medium} to give a 95\% signal efficiency. Likewise the \textit{Tight} menu is obtained by tightening the cuts in the \textit{Medium} menu to give a signal efficiency of 75\%. In addition to these \textit{Multilepton} menu was also defined, which is optimized for low energy electron selection in the $\text{H}\rightarrow ZZ^* \rightarrow 4l$ analysis. The \textit{multilepton} menu has a similar efficiency as the \textit{Loose} menu but a better background rejection. 

\begin{table}
    \centering
    \begin{tabular}{l|l|l|l|l}\hline
        Name & \textit{loose} &\textit{medium} &\textit{tight} &\textit{multilepton}\\\hline\hline
        $R_\text{had1}$ & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $f_3$           &               & $\checkmark$  & $\checkmark$  & $\checkmark$  \\\hline\hline
        $W_{\eta^2}$    & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $R_\eta$        & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $R_\phi$        &               &               &               &               \\\hline\hline
        $w_{s,tot}$     & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $E_\text{ratio}$& $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $f_1$           &               &               &               &               \\
        $f_1(\text{core})$&             &               &               &               \\\hline\hline
        $n_\text{Blayer}$&              & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $n_\text{Pixel}$&$\checkmark$   & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $n_\text{Si}$   &$\checkmark$   & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $n_\text{TRT}$  &               & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $f_\text{HT}$   &               & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $d_0$           &               & $\checkmark$  & $\checkmark$  &               \\
        $\sigma_{d_0}$  &               &               &               &               \\\hline\hline
        $\Delta P/P$    &               &               &               &               \\
        $\Delta\phi_\text{res}$&        &               &               &               \\\hline\hline
        $\Delta\eta1$   & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  \\
        $\Delta\phi2$   &               &               &               & $\checkmark$  \\
        E/p             &               &               & $\checkmark$  &               \\
        convBit         &               &               & $\checkmark$  &               \\\hline\hline
    \end{tabular}
    \caption{Variables used in various electron cut menus. The \textit{loose} uses only a minimum set of variables while the \textit{medium}, \textit{tight} and \textit{multilepton} menus use a host of variables.}
    \label{tab:electron_var}
\end{table}



During the 2012 data taking period the instantaneous luminosity of the LHC was quite high compared to the 2011 data run. Due to this increased luminosity, the average interactions per bunch crossing almost doubled. Certain optimizations were added to the cut menu to make it less susceptible to pileup. The pileup stability of the operating points are shown in Figure~\ref{img:isEm_pileup_stable}. The Z-boson reconstruction part of this thesis was performed with this improved pileup stable cuts menu. The electron variables used in the menu are described in Table~\ref{tab:electron_var}.

\section{Conclusion}
Using the ID and calorimeter variables, electron identification capabilities of ATLAS have been demonstrated. Several standard working points which are optimized for the selection of isolated electrons has been described. However heavy flavor decay electron or soft electron identification is not optimized in this identification scheme. A method to identify soft electrons is described in the following chapter.

%\bibliographystyle{atlasBibStyleWithTitle}
%\bibliography{chap7_electron_reconstruction}

\chapter{Experimental setup}\label{chap:setup}

In this chapter a brief overview of the Large Hadron Collider (LHC) as well as a description of \textbf{A} \textbf{T}oroidal \textbf{L}HC \textbf{A}pparatu\textbf{S} (ATLAS) detector is given. In Section~\ref{section:lhc} a brief overview of the pre-accelerating system as well as the LHC machine is given and in Section~\ref{section:atlas_detector} an overview of the ATLAS detector and its various components are described.

\section{The Large Hadron Collider}
\label{section:lhc}
The Large Hadron Collider (LHC) is one of the largest scientific instrument in the world at present. It is located at the European Organization for Nuclear Research(CERN), across the border of France and Switzerland. A scheme of the machine is given in Fig.~\ref{image:lhc_scheme}. A detailed review of LHC can be found in~\cite{Pettersson:291782}

The accelerator is located in a circular tunnel of circumference 27 km. The LHC accelerates proton bunces in opposite directions in separate beam pipes. The bunches are crossed at four interaction points at the center of four LHC experiments namely ALICE~\cite{Aamodt:2008zz}, ATLAS~\cite{Aad:2008zzm}, CMS~\cite{Chatrchyan:2008aa} and LHCb~\cite{Alves:2008zz}. Proton collisions happen during bunch crossings and the detectors detect the particles produced in the collissions. The machine is designed to  accelerate protons up to 7 TeV per beam, thus producing a center of mass energy of $\sqrt{s}$ = 14 TeV.  
    
    \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{./images/chap4/lhc_scheme.eps}
    \caption{Sketch of the locations of the four main experiments(ALICE,ATLAS,CMS and LHCb). The depth of the tunnel varies from 50 m to 150 m underground. Image~\cite{Jean-Luc:841555}}
    \label{image:lhc_scheme}
\end{figure}

The acceleration sequence of protons starts with a linear accelerator called Linac2. Hydrongen gas is fed to a duoplasmatron which gives $\text{H}^+$. These ions are accelerated by the Linac2 to 50 MeV. %The only valence electron of hydrogen atoms are ``stripped'' of and fed to the Linac2, which accelerates the ``naked'' hydrogen atoms (protons) to 50 MeV.
They are then injected into the Proton Synchrotron Booster which accelerates them up to \SI{1.4}{\giga\electronvolt}. After this acceleration step the protons are sent to the Proton Synchrotron (PS) where the proton energy is increased to \SI{25}{\giga\electronvolt}. Subsequently they are sent to the Super Proton Synchrotron (SPS) to accelerate them up to \SI{450}{\giga\electronvolt}. Finally, these protons are injected into the LHC where the final acceleration takes place. The proton beams circulate the ring in bunches. Under nominal operating conditions, each proton beam consists of 2808 bunches, with each bunch containing about $10^{11}$ protons. 

The design energy of LHC is $\sqrt{s}$ = \SI{14}{\tera\electronvolt}. However during a powering test in 2008, an electrical fault occurred producing electrical as well as mechanical damages. The reason for the fault was found out to be a faulty solder connection~\cite{Bajko:1168025}. Several problematic connections were repaired in several other magnets. Until further repairs could be made it was decided to run the LHC at a reduced energy. In 2010 and 2011 the LHC was operated at \SI{3.5}{\tera\electronvolt} per beam, producing $\sqrt{s}$ = \SI{7}{\tera\electronvolt} collisions. In 2012 the energy was increased to \SI{4}{\tera\electronvolt} per beam, producing $\sqrt{s}$ = \SI{8}{\tera\electronvolt} collisions. The LHC is shutdown during 2013 and 2014 for a series of repairs as well as detector upgrades. The machine is expected to run at $\sqrt{s}$ = \SI{13}{\tera\electronvolt} during the post repair startup expected in 2015. Major part of this thesis uses the $\sqrt{s}$ = \SI{8}{\tera\electronvolt} data taken during 2012 run period. Data recorded at $\sqrt{s}$ = \SI{7}{\tera\electronvolt} during the 2011 run period has been used for the analysis in chapter~\ref{chap:l1calo}

\subsection{Luminosity}
The event rate R in a collider experiment is proportional to the interaction cross section $\sigma_\text{int}$~\cite{accelerator.pdg}. The relationship is given by 
\begin{align}
    \mathrm{R} &= \mathrm{L}\sigma_\text{int}.
\end{align}
The proportionality constat L is called the luminosity. If $n_1$ and $n_2$ are the number of particles in two colliding bunches which collide at a frequency $f$, the luminosity is 
\begin{align}
    \mathrm{L} &=f\frac{n_1n_2}{4\pi\sigma_x\sigma_y}.
\end{align}

Where $\sigma_x$ and $\sigma_y$ are the transverse beam profiles in the horizontal and vertical direction. The beam profiles can be expressed in terms of two quantities namely \textit{transverse emittance}, $\epsilon$ and \textit{amplitude function}, $\betaup$.  The beam quality parameter $\epsilon$ reflects the process of bunch preparation. The $\betaup$ function is a beam optics quantity that is determined by the magnet configuration. The transverse beam profile $\sigma$ is related to $\epsilon$ and $\betaup$ through 

\begin{align}
    \epsilon &=\pi\frac{\sigma^2}{\betaup}
\end{align}

With $\betaup$ and $\epsilon$ the expression for luminosity becomes
\begin{align}
    \mathrm{L} &=f\frac{n_1n_2}{4\sqrt{\epsilon_x\betaup_x^*\epsilon_y\betaup_y^*}}
\end{align}

where $\betaup^*_{x,y}$ is the $\betaup$ function at the interaction point.


In order to increase the production rate of rare physics events, the luminosity has to be increased. This can be achieved by operating the accelerator with a great number of protons per bunch with low emittance and low $\betaup$ function at collision point. 

The instantaneous luminosity as well as integrated luminosity delivered by LHC and recorded by ATLAS detector is shown in Figure~\ref{img:lumi}
\begin{figure}[h!]
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{./images/chap4/lumivstime.eps}
        \caption{}
    \end{subfigure}

    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap4/intlumivstime2011-2012DQ.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap4/mu_2011_2012-dec.eps}
        \caption{}
        \label{img:pileup}
    \end{subfigure}
    \caption{(a) peak instantaneous luminosity recorded by ATLAS (b) Total integrated luminosity delivered by LHC and recorded by ATLAS (c) ``pile up'' or average interactions per bunch crossing for 2011 and 2012 run periods}
    \label{img:lumi}
\end{figure}

With increasing luminosity, the average number of interactions per bunch crossing, $\mathrm{\muup}$ also increases.
\begin{align}
    \muup &= \text{L}_\text{int}\frac{\sigma_\text{inel.}}{fn_b}
\end{align}

where $\text{L}_\text{int.}$ is the integrated luminosity, $\sigma_\text{inel}$ is the proton-proton ineleastic cross section and $n_b$ is the number of bunches per beam. The quantity $\muup$ is also a measure of the number of multiple interactions per bunc crossing or ``pile up''. The distribution of $\mathrm{\muup}$ is shown in Figure~\ref{img:pileup}.

\section{ATLAS detector}
\label{section:atlas_detector}
The ATLAS detector is located at one of the interaction points in the LHC. A three dimensional view of the detector is shown in Figure.~\ref{image:atlas_cutaway}. The detector is 25 m in diameter, 45 m in length and weighs about 7000 tonnes. 
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{./images/chap4/ATLAS_SE_Corrected7.eps}
    \caption{Cut away view of the ATLAS detector. A cartoon of two human beings are also included to highlight the size of the detector}
    \label{image:atlas_cutaway}
\end{figure}

The detector is forward backward as well as cylindrically symmetric. The beam pipe is located along the longitudinal cylindrical axis. The nominal proton-proton collision point is at the center of the detector where the cylidrical axis intersects with the forward-backward symmetry axis. This point defines the co-ordinate origin of the ATLAS detector system.  

\subsection{Co-ordinate system}
The ATLAS co-ordinate system is a right handed co-ordinate system with the positive x-axis defined as pointing from the interaction point to the center of the LHC ring. The positive y-axis is defined as pointing upwards. The side A of the detector is defined as the positive z side and side C is defined as the negative z side. The cylindrical coordinates r,$\phi$ are used in the transverse plane with the azimuthal angle $\phi$ measured around the longitudinal axis along the beam line. The polar angle $\thetaup$ is measured as the angle from the longitudinal axis. The pseudorapidity $\etaup$ is defined as $\etaup$ = -$\ln[\tan(\thetaup/2)]$. The distance measure $\Delta R$ is defined in the $\etaup-\phi$ space, as $\Delta R=\sqrt{\Delta\etaup^2 + \Delta\phi^2}$
\subsection{Inner Detector}

At design luminosity, proton-proton collisions produce approximately 1000 particles every 25 ns within the central region of the detector ($|\etaup| < 2.5$). The inner detector (ID) has been designed to make precision measurements at this high track densities~\cite{atlasID_tdr}. The ID is immersed in a solenoidal magnetic field of 2 T. This allows for momentum measurements as well as the separation of charged particles and neutral particles. 
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.35]{./images/chap4/FigID11alast.eps}
    \caption {Graphics showing a charged particle track (red) passing through various sub-detectors of the inner detector. The particle traverses the beryllium beam pipe, 3 cylindrical pixel layers, 4 double layers of silicon-microstrip sensors (SCT) and approximately 36 straw tubes of the transition radiation tracker (TRT)}
    \label{image:ID_cutaway}
\end{figure}


The ID consists of three complimentary sub-detectors namely pixel detector~\cite{Wermes:381263}, silicon microstrip (SCT) detector ~\cite{Abdesselam:2006wt} and transition radiation tracker (TRT)~\cite{1748-0221-3-02-P02014}. A schematic of the arrangement of these sub-detectors around the beam pipe is shown in Figure~\ref{image:ID_cutaway}.

The pixel detector as well as the SCT are based on silicon technology. This allows for high granularity and very good position resolution due to their small size. The sensor is essentially a reverse biased p-n junction. The depletion region acts as the active detecting region. When a charged particle passes through the depletion region, electron-hole pairs are formed and they drift towards the respective electrodes. They are registered as signals at the electrodes and can be read out through output channels.

The pixel detector is the innermost component of the ID. Over three barrel layers and three end-cap disks it has over 80 million output channels with cell sizes of $50~\muup\text{m}\times400~\muup\text{m}$ and $50~\muup\text{m}\times600~\muup\text{m}$. It has a position resolution of $10~\muup\text{m}$ in the r-$\phi$ plane and $115~\muup\text{m}$ along the Z direction. 

The SCT is a concentric detector that surrounds the pixel detector. The SCT layers are composed of double layers of silicon strips. The dimensions are $80\muup\text{m}\times6\text{cm}$. The construction of the double layers is such that they are rotated by a stereo angle of \SI{40}{\milli\radian} with respect to each other. The spatial resolution of the SCT is $17~\muup\text{m}$ in $r-\phi$ and \SI{580}{\micro\meter} along Z direction. Even though the strip length is \SI{6}{\cm} a very good position resolution in Z direction is gained due to the stereo angle between the double layers. There are approximately 6 million read out channels for the SCT. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{./images/chap4/approv-ToTsumLT01vsP_data-label.eps}
    \caption{Time over threshold estimator showing particle identification capabilities of the TRT as a function of momentum}
    \label{img:trt_pid}
\end{figure}


The TRT is a straw-tube tracker. It consists of approximately 300,000 straw-tubes which can give a position resolution in $\phi$ of $130~\muup\text{m}$. The TRT can produce up to 36 space points per charged particle. In addition it is also capable of particle identification. Charge particles while traversing different media emit radiation. Since the relativistic $\gammaup$ factor is larger for an electron compared to a hadron, electron emits more transition radiation than a hadron. The TRT read out system records two signal thresholds, high and low. The information about the number of high threshold and low threshold hits can be used to separate hadrons and electrons. Figure~\ref{img:trt_pid} shows a time over threshold estimator derived from TRT hits, which shows excellent particle identification capabilities. The momentum resolution of ID $\Delta\pt/\pt = 0.04\%\times\pt\oplus2\%$ ($\pt$ in GeV)~\cite{Aad:2009wy}. 


\subsection{The Calorimeter System}
Calorimeter systems are used to measure the energy of particles. When a particle is incident on a material, depending upon its energy it undergo various interactions. As it can be seen from Figure~\ref{img:interactions}, electrons and positrons above \SI{10}{\mega\electronvolt} predominantly undergo bremsstrahlung, whereas photons undergo pair production. The bremsstrahlung photons as well as pair produced electrons and positrons produce secondary particles with progressively decreasing energies through the same mechanism until a cut-off energy is reached. This production of cascade of particles is called a shower. For a comprehensive overview, see~\cite{RevModPhys.75.1243}.

\begin{figure}[h!]
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap4/photon_interaction.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap4/electron_interaction.eps}
        \caption{}
    \end{subfigure}
    \caption{Cross sections for various interactions with matter for (a) photons and (b) electrons and positrons~\cite{nakamura2010review}}.
    \label{img:interactions}
\end{figure}

There are two types of particle showers; electromagnetic and hadronic. The longitudinal as well as lateral features of an electromagnetic shower can be parameterized through a quantity called  radiation length $\mathrm{X_0}$ which depends on the type of the material. The radiation length ($\mathrm{X_0}$) is defined as the length at which a high energy electron looses all but 1/e of its energy or it is 7/9$^\textrm{th}$ of the distance at which the intensity of a photon beam is reduced to 1/e of its initial value while propagating through a given material. 

Hadronic showers are more complicated than electromagnetic showers. Secondary hadrons are produced through successive strong interactions with a ``nuclear interaction length'' $\lambdaup_0\approx 35\mathrm{A^{1/3}}$\SI{}{\gram\per\square\cm} (where A is the mass number of the material). A significant fraction of the primary energy is used in nuclear excitations, nucleon evaporations and spallations. Particles like neutral pions decay into photons which interact with the material only through electromagnetic interaction. Thus energy from the hadronic part is transferred to the electromagnetic part. Typically the figure of merit of hadronic calorimeters are quantified through the ratio of electromagnetic response to hadronic response. 

The principle of calorimetry lies in measuring the energy deposited by particle showers through which the energy of the incident particle can be inferred. Since the shower lengths increases only logarithmically with respect to the energy of the particle, with a small detector volume very high energy particles can be measured. In addition the energy resolution is proportional to $\mathrm{1/\sqrt{E}}$, therefore at high energies the relative resolution gets better as opposed to magnetic spectrometers. Calorimeters are sensitive to all kinds of particles, charged or otherwise. Through the reconstruction of missing energy, one can even measure the energy of a neutrino in an event.  


\begin{figure}
    \centering
    \begin{subfigure}{\textwidth}
        \includegraphics[height=0.45\textheight]{./images/chap4/Calorimeter_d3.eps}
        \caption{}
        \label{img:calorimeter_cutaway}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[height=0.45\textheight]{./images/chap4/LARG3-TDR-barrelM.eps}
        \caption{}
    \end{subfigure}
    \caption{(a) Cut away view of the ATLAS calorimeter system (b) Sketch of a LAr barrel module showing different calorimeter layers with varying granularity in each layers.}
\end{figure}

The ATLAS calorimeters (Figure~\ref{img:calorimeter_cutaway}) are ``sampling'' calorimeters. These are special kind of calorimeters in which the active materials are interleaved with passive materials. The passive layer is a low radiation length material, which is responsible for producing particle showers, while the active layer detects these showers and gives a measure of the particle energy. The full energy of the incident particle is inferred from a sample of the energy detected by the active layer.

The ATLAS electromagnetic calorimeter is a Lead--liquid argon(LAr) sampling calorimeter with accordion geometry~\cite{ATLAS:1996ab}, which provides full coverage in $\phi$ and  $|\etaup|<3.2$. Incident particles produce showers in the low radiation length lead material. These shower particles causes ionization in the LAr medium. Argon is a noble gas which has a boiling point of \SI{87}{\kelvin}. The low operating temperature reduces outgassing from the liquid containment and hence reduces contamination during operation thus ensuring high charge collection efficiency.

The thickness of the Lead layer ranges from 1.1--\SI{2.2}{\mm} in $\etaup$. A high voltage of \SI{2}{\kilo\volt} is applied across the LAr gap of $\approx$ \SI{4}{\mm}. This causes the drifting of the electrons towards the readout electrodes with a drift time of $\approx$\SI{400}{\nano\s}. This is longer than the LHC bunch crossing structure of \SI{25}{\nano\s}. The accordion geometry where the gaps and absorbers are at an angle along the direction of the incoming particle allows for a fast charge collection as well as prevent the escape of particles through the gaps. 

The LAr calorimeter has three layers in the longitudinal direction. The first layer has a pitch of \SI{4}{mm} in $\etaup$. This fine granularity allows for electron pion separation. The second layer has a granularity of 4$\times$\SI{4}{\cm\square}. The third layer has a granularity of 8$\times$\SI{4}{\cm\square}. The overall thickness of the electromagnetic calorimeter is over 24 radiation lengths in the barrel region and over 26 radiation lengths in the end-caps. The energy resolution of the electromagnetic calorimeter $\Delta E/E = 11.5\%/\sqrt{E}\oplus0.5\%$

The hadronic sampling calorimeter uses scintillator tiles as well as LAr technology. The scintillator tile calorimeter is separated into a central barrel ($|\etaup| < 1.6$ and two extended barrels (in the range $|\etaup|<1.7$). The end-caps ($1.5 < |\etaup| < 3.2$) receives a higher radiation dose than the barrel, therefore radiation hard liquid argon technology is used in this region. The central region is also called TileCal (Tile calorimeter). It uses scintillating tiles as the active material and steel plates as absorbers. The scintillating tiles are coupled to wavelength shifting fibers and the signal are read out using photomultiplier tubes.

\subsection{Muon Spectrometer}
The muon spectrometer is the outermost detector element of the ATLAS  detector~\cite{ATLAS:1997ad}. It measures the position of muons as they propagate through the spectrometer. The bremsstrahlung energy losses of a charge particle is inversely proportional to its mass ($1/\text{m}^4$). Since the muon is roughly 200 times more massive than the electron, they easily pass through the calorimeter and reach the muon system.
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{./images/chap4/MuonSystem_d3.eps}
    \caption{The ATLAS muon spectrometer}
\end{figure}

The spectrometer is a immersed in a torroidal magnetic field. Air core torroidal magnets are used to reduce the multiple scattering. In the region $|\etaup| < 1.4 $ (barrel region), magnetic bending is provided by the barrel torroidal magnet. Between $1.6 < |\etaup| < 2.7$ (end-cap region), the magnetic field is provided by end-cap torroidal magnets. In the transition region ($1.4<|\etaup|<1.6$), the magnetic field is provided by a combination of barrel and end-cap torroid fields. The magnet configuration provides a field which is mostly orthogonal to the particle trajectories.

Monitored Drift Tubes (MDT) chambers and Cathode Strip Chambers(CSCs) make precision measurements in $\etaup$. Thin Gap Chambers (TGC) and Resistive Plate Chambers (RPCs) provide low granularity fast signals to the trigger system. The target momentum resolution of the muon system is 10\% for \SI{1}{\tera\electronvolt} muons.

%\bibliographystyle{atlasBibStyleWithTitle}
%\bibliography{chap4_experimentalsetup}

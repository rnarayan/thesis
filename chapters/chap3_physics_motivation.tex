\chapter{Physics at the LHC}\label{chap:theory}

The measurements in this thesis are based on proton-proton collisions. The proton is a composite particle made up of two up quarks and one down valence quarks, which are bound together by the strong interaction. Proton-proton collisions are effectively collisions between the proton constituents at high collision energies. In this chapter a brief review of the structure of the proton and physics at hadron colliders is given. The physics of the associated production of Z-bosons with b-quarks and their final state is also reviewed in the later sections of the chapter.



\section{Structure of the Proton}

The study of collision processes by observing the energy, angular and multiplicity distribution of scattered products of the collision has been successfully employed in understanding the nature of matter and its interactions at small scales, ever since Rutherford's $\alphaup$-scattering experiment.

In classical Coulomb scattering experiments, particles like protons were considered to be point like. In later experiments with increased energies, a deviation from point like behavior was observed and the size and structure of the proton became apparent.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{./images/chap3/dis.eps}
    \caption{Schematic representation of Deep Inelastic scattering. An electron with four momentum k interacts with hadron with momentum P.}
    \label{img:dis}
\end{figure}

The structure of the proton was revealed by the scattering of a lepton off a proton. These experiments are called ``deep inelastic scattering'' (DIS) experiments. A typical process would be $eP\rightarrow e^\prime X$, where $e$ and $e^\prime$ are the incident and outgoing electrons, P is the proton and X denotes all the final state hadrons. The kinematics of the scattering depends on the momentum transfer $q = k - k^\prime$ from the electron to the proton and the invariant mass of the hadronic final state. The variables that describe DIS are the following. 
\begin{align}
    Q^2 &\equiv -q^2\\
    \nu &= P\cdot q
\end{align}
and 
\begin{align}
    x &= \frac{Q^2}{2M\nu}\\
    y &= \frac{q\cdot P}{k\cdot P}
\end{align}

where $Q^2$ is the negative square of momentum transfer $q$. $\nu$ is the energy transferred to the hadron in its rest frame. $x$ is the fraction of momentum carried by the parton, also known as Bjorken-$x$ and $y$ is the fraction of energy lost by the electron in the nucleon rest frame.

The inelastic electron-proton scattering cross section can be written as 
\begin{align}
    \frac{d^2\sigma}{dx\,dQ^2} &= \frac{4\pi\alpha^2}{xQ^4}\left[(1 - y)F_2(x,Q^2) + xy^2F_1(x,Q^2)\right]
\end{align}

where $F_1$ and $F_2$ are the structure functions of the proton.

The structure of the proton (or in general any hadron) is described by the parton model. According to the parton model hadrons are collection of quasi-free, point like particles called partons. Hadron-hadron collision cross sections are modelled as the sum of the cross sections of individual partons distributed within each hadron that participate in the collision. 

In the naive parton model, the proton consists of point like massless partons which do no interact with each other. Each of them carry a momentum $p_i^\mu = x_iP_i^\mu$. The transverse momenta of individual partons are neglected.

According to this model, the structure functions hold the relation:
\begin{align}
    2xF_1(x)\equiv F_2(x) =\sum_ie_i^2f_1(x) + \mathcal{O}(\alphaup(Q)),
\end{align}
where $f_1(x)$ are the parton distribution function and $e_i$ is the parton charge. To the first order, the structure functions are independent of $Q^2$, a property known as Bjorken scaling.  Even though the parton model offers a very good description of the proton, experiments suggest that only half of the momentum is carried by charged particles. The remaining neutral component can be attributed to the gluons. 

\begin{figure}[t]
    \centering
    \includegraphics[width=0.8\textwidth]{./images/chap3/gluon_emission.eps}
    \caption{Splitting graphs responsible for violation of Bjorken scaling.}
    \label{img:splitting_graphs}
\end{figure}



\subsection{Parton Distribution Functions}

Parton distribution functions (PDFs) are defined as the probability of finding a particle within a longitudinal momentum fraction $x$ and $x+dx$, at a given energy scale $Q^2$. Parton distribution functions cannot be obtained from perturbative calculations. Hence they need to be obtained from fits to the data. In the Figure~\ref{image:h1_pdf}, HERA PDF's are shown. Here the PDF is parameterized at a factorization scale of \SI{1.9}{\giga\electronvolt\square} and evolved to \SI{10}{\giga\electronvolt\square}. To make any meaningful predictions about the hard processes, a precise knowledge of the PDF's are essential.


\begin{figure}[h!]
    \centering
    \includegraphics[height=0.5\textheight]{./images/chap3/herapdf15f_alpha.png}
    \caption{The parton distribution functions from HeraPDF~\cite{CooperSarkar:2011aa} at \mbox{$Q^2=\SI{100}{\giga\electronvolt}$} as a function of momentum transfer $x$. Gluon and sea quark distributions are scaled down by a factor of 20.}
    \label{image:h1_pdf}
\end{figure}



In the QCD improved parton model, the gluon radiation gives small transverse momenta to the quarks. As a result the structure function is modified.
\begin{align}
    \frac{F_2(x,Q^2)}{x} & = e^2\int_x^1\frac{dy}{y}f(y)\left[\delta\left(1- \frac{x}{y}\right) + \frac{\alphaup}{2\pi}P_{qq}\left(\frac{x}{y}\right)\ln\frac{Q^2}{m^2}\right].
\end{align}
where $P_{qq}(z)$ is the splitting function, which represents the probability that a parent quark emits a gluon with a momentum $(1 - z)$. The feynman graphs for the splitting is shown in Figure~\ref{img:splitting_graphs}. The term $f(y)$ is the parton distribution function, which is the probability of finding a parton with a longitudinal momentum fraction between $x$ and $x+dx$. 

The parameter $m$ is a cut-off parameter. A cut-off scale $\mu$, called factorization scale is introduced to avoid singularities as $m\rightarrow 0$. The parton distribution function can now be written as 
\begin{align}
    f(x,Q^2) &= f(x,\mu^2) + \frac{\alphaup_s}{2\pi}\int_x^1\frac{dy}{y}f(y,\mu^2)P_{qq}\left(\frac{x}{y}\right)\ln{\frac{Q^2}{\mu^2}} + \mathcal{O}(\alpha_s^2)
\end{align}


The evolution of the parton distribution function as a function of $Q^2$ at a known $x$ and a particular choice of factorization scale $\mu$, the dynamics, is described by the Dokschitzer-Gribov-Lipatov-Altarelli-Parisi (DGLAP) equation

\begin{align}
    \frac{\partial f(x,\mu^2)}{\partial \ln \mu^2} &=\frac{\alpha_s}{2\pi}\int_x^2\frac{dy}{y}f(y,\mu^2)P_{qq}\left(\frac{x}{y}\right) + \mathcal{O}(\alpha_s^2)
\end{align}

As $Q^2$ increases, more splittings are resolved resulting in low momentum quarks. As a result, the quark distributions increases at low values of $x$ with increasing $Q^2$. This $Q^2$ dependance is manifested as scaling violations in the structure function distribution shown in Figure~\ref{img:scale_violation}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{images/chap3/scaling_violation.eps}
    \caption{Structure function $F_2$ measured as a function of $Q^2$ at Hera experiments H1 and ZEUS and CERN experiments NMC and BCDMS. At low $x$ values the structure function shows a strong $Q^2$ dependence indicating violation of Bjorken scaling~\cite{Ahmed1995471}.}
    \label{img:scale_violation}
\end{figure}



\subsection{Proton-Proton Collisions}
At high energy hadron colliders two types of processes can be distinguished, the hard process which can be calculated precisely using perturbative QCD and soft processes which cannot be well described by QCD. Phenomenological models exists to describe the soft processes. Even though our interest is in the hard process, it is invariably followed by the soft processes.

The QCD factorization theorem separates the short-distant process like hard scattering, initial and final state radiations from long-distant properties like hadronization evolution of parton distribution functions.  According to the factorization theorem the cross section for two interacting protons can be written as the convolution of cross sections of the partonic content of the proton. 
\begin{align}
\sigma(s) &=\sum_{i,j}\int dx_1\, dx_2\,f_i(x1,\mu^2)f_j(x_2,\mu^2)\hat{\sigma_{ij}}\left(\hat{s},\alphaup_S(\mu^2),\frac{Q^2}{\mu^2}\right),
\end{align}
where $\mu$ is the factorization scale, $\hat{\sigma_{ij}}$ is the partonic cross section for partons $i,j$ and $f_i$ are the parton distribution functions. $\mu$ can be thought of as the scale which separates long and short distance physics. A parton with transverse momentum less than $\mu$ is considered to be the part of the hadron structure and it is absorbed in the parton distribution function. Partons above this scale participate in the hard scattering with a cross section $\hat{\sigma}$. The choice of the scale $\mu$ depends on the process that is considered. 


\section{Associated Production of Z-bosons with b-quarks}
\begin{figure}
    \begin{subfigure}{0.3\textwidth}
        \includegraphics[width=\textwidth]{./images/chap3/zbb_1.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.3\textwidth}
        \includegraphics[width=\textwidth]{./images/chap3/zbb_2.eps}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.3\textwidth}
        \includegraphics[width=\textwidth]{./images/chap3/zbb_3.eps}
        \caption{}
    \end{subfigure}
    \caption{Leading order diagrams for the production of Z-boson in association with two b-quarks.}
    \label{img:zbb_lo-models}
\end{figure}

Electroweak gauge bosons in association with one or more b-quarks are produced at LHC with large cross section. These processes are background to many New Physics searches at LHC, 
For example, the Higgs boson decay process $H\rightarrow Z^*Z^*\rightarrow$ where the two virtual Z-bosons decay into leptons is an important signature for Higgs measurement. The process $pp\rightarrow Zb\bar{b}$ with the leptonic decay of the Z-bosons and the semileptonic decay of the b-quarks constitute a large fraction of background for this process. Therefore a very good understanding of these processes are required to increase the chances of observing New Physics signals. The leading order Feynman graph contributing to the production of Z boson with associated b quarks are given in Figure~\ref{img:zbb_lo-models}. 

There are basically two different schemes for the cross section calculation of the process, the fixed flavor scheme and variable flavor scheme. In the fixed-flavor scheme the quark flavors are kept fixed irrespective of the energy scales involved. In this scheme there are three flavor (u,d,s), four flavor (u,d,s,c) as well as five flavor (u,d,s,c,b) schemes available.

In the variable flavor scheme, the number of quark flavors considered is given by a step function as a function of the energy scale. At hard scales above the mass of the heavy quarks the heavy-flavor PDF's are used in the calculation of the process. 

For the calculation of $Z+b\bar{b}$ process, the four flavor as well as the five flavor number schemes can be used. In the four flavor scheme, the b-quarks are generated dynamically by gluon splitting ($g\rightarrow b\bar{b}$).In the five flavor scheme, since the Z-mass scale is well above the production threshold of b-quarks, initial states with all the flavors upto b-quarks are parameterised and evolved in $Q^2$. A collinear $g\rightarrow b\bar{b}$ splitting is assumed with one of the b-quark escaping down the beam line and the other quark participating in the hard scattering. The b-quarks are treated as originating from the proton sea. The leading order and next to leading order QCD calculations differ from each other due to poorly constrained b-PDF's~\cite{PhysRevD.73.054007}. The b-quark distribution functions can be derived perturbatively from the DGLAP evolution equation. The process $gb\rightarrow Zb$ is sensitive to gluon density of the proton.

The life-time of the Z-boson is about \SI{d-25}{\second}. It decays into fermion anti-fermion pairs. The decay into top-quark pairs is heavily suppressed due to the high mass of the tops. The branching ratio of Z-boson decays is listed in Table~\ref{tab:z_boson_BR}. Even though the branching fraction for Z-boson decays into hadrons are higher, the lepton final states with electrons and muons offer a very clean signature at hadron colliders.

\begin{table}
    \centering
    \begin{tabular}{cl}\hline
        Final state & BR \% \\\hline
        $e^+e^-$ & $3.363\pm0.004$\\
        $\mu^+\mu^-$ &$3.366\pm0.007$\\
        $\tau^+\tau^-$&$3.367\pm0.008$\\
        neutrinos &20.06\\
        hadrons & 69.91\\\hline
    \end{tabular}
    \caption{Branching ratios of the Z-boson~\cite{Beringer:1900zz}.}
    \label{tab:z_boson_BR}
\end{table}


\subsection{B-hadron Production}
\begin{figure}
    \includegraphics[width=\textwidth]{./images/chap3/hadronization.eps}
    \caption{Schematic representation of production of Z-boson in association with b-quark pairs and subsequent fragmentation and hadronization and decay of heavy flavor hadrons.}
    \label{img:b_production}
\end{figure}
The schematic representation of B-hadron production is shown in Figure~\ref{img:b_production}. In this scheme, the two quarks from the incoming protons interact in the hard scattering and produces a gluon. This gluon emits two b-quarks of which one radiates a Z-boson. The Z-boson subsequently decays into an electron positron pair. The b flavoured quark and anti-quark continue to separate from the primary interaction point. Due to QCD confinement the strong force increases with increasing separation until a quark anti-quark pair is produced from the vacuum. These quarks can radiate gluons which in turn split into quark anti-quark pair. This cascade of quark and gluon production process known as parton-showering continues to take place until the energies of the partons reaches a particular scale. At that scale the partons combines to form bound states which are called hadrons. These hadrons decay into lighter particles, either through strong or weak interactions. 

In addition to the hard scattering, b-quarks can also be produced through gluon splitting in the parton shower. The rate of production is B-hadrons via gluon splitting is quite high in high energy hadron colliders. Eventually this becomes the dominant background in this study.

Since it is energetically more favourable for a b or $\bar{b}$ quark to combine with another single quark from vacuum, about 90\% of the times B mesons ($B_o$, $\bar{B^0}$, $B^+$, $B^-$) are the final hadronized products in the chain of showering of b flavoured quarks. Due to their high mass the formation of strange and charm quarks during parton showering is relatively suppressed compared to lighter quarks. Therefore $B_s^0$ production constitute approximately 10\% of the b-flavoured final states. 

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{./images/chap3/spectatorquark.eps}
    \caption{Spectator quark diagram for B meson decay.}
    \label{img:spectator_quark}
\end{figure}

The ground state b hadrons decay further to lighter hadrons through charged current interaction. The b-quark in the meson carries away a significant fraction of the momentum of the meson. Therefore it is reasonable to assume that its decay is independent of light quarks in the hadron. The semileptonic decay of B-hadron in such a spectator quark model is shown in Figure~\ref{img:spectator_quark}. The branching fraction for this direct semileptonic decay is \SI{10.86d-2}. Cascaded decay of the b-quark ($b\rightarrow c\rightarrow e$) also contribute to the semileptonic decay final states. The inclusive branching fraction for the decay with at least one electron in the final state is $19.3\pm0.5\times10^{-2}$. The energies of these electrons are less compared to those coming from Z-boson decays, hence they are also called ``soft'' electrons.

In this thesis, a pair of 2 opposite sign electron final states are probed which is compatible with the decay topology of a Z-boson into $e^+,e^-$ and the two $b\bar{b}$ decay into $e^+,e^-$. The kinematics and environment in which these two categories can be potentially observed is quite different, However with the ATLAS detector, identification of these final states is possible.

%\bibliographystyle{atlasBibStyleWithTitle}
%\bibliography{chap3_physics_motivation.bib}

\chapter{Level-1 Calorimeter Trigger Monitoring}\label{chap:l1calo}

The event rates at hadron collider experiments like ATLAS are so high that existing data acquisition systems cannot record and store informations from all the events. Therefore only interesting events are recorded. The decision about which event to record and which one to discard is made by the so called trigger system. In this chapter the ATLAS trigger system with an emphasis to the authors ATLAS service task related to the timing monitoring of the calorimeter based trigger system is briefly reviewed.

\section{ATLAS Trigger System}
Many physics processes of interest at the LHC have a low cross section. This means the machine has to be operated at high event rates in order to increase the statistics of interesting events. This directly translates to the luminosity of the machine. The design luminosity of the LHC is \SI{d34}{\per\cm\squared\per\second}~\cite{Brüning:782076}. At these luminosities, roughly one billion proton-proton collisions would occur every second. However the ATLAS detector is designed to record only 200 events per second. Since this rate is  5 million times smaller than what is produced, only events of interest can be recorded. 

The ATLAS detector has two levels of hardware triggers namely Level-1 (L1) and Level-2 (L2) and a software based Event Filter (EF). The L1 trigger processes coarse (reduced granularity) detector signals to arrive at trigger decision. The L1 has two subsystems, namely the Level-1 Calorimeter trigger (L1Calo) and Level-1 Muon trigger (L1Muon). L1Calo processes information from the calorimeter systems, mainly looking for signatures of isolated high transverse-energy electrons, photons, tau's, jets and missing $\et$. L1Muon processes information from the Muon system. 

The time interval between proton-proton bunch crossings at the LHC is \SI{25}{\nano\s}. This interval is very short to make a decision on whether to  accept or discard the event. Therefore the detector data is held in memory buffers while L1 makes a trigger decision. Once a successful decision is made, the trigger system sends a Level-1 Accept (L1A) signal to the buffer memories and the event is accepted and read out by the data acquisition system, otherwise it is discarded. The time duration for which the event has to be kept in these buffers is limited by the volume of the buffers. In ATLAS this duration is $2.5~\muup\text{s}$. Therefore the L1 has to make a trigger decision within $2.5~\muup\text{s}$. For a safer margin this latency period has been set to $2~\muup\text{s}$.

\section{L1Calo Trigger}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{images/chap5/l1calo_scheme.eps}
    \caption{A schematic representation of the L1Calo trigger system. Black arrows show real time data path, and grey arrows show readout data path \citep{1748-0221-3-03-P03001}}
    \label{fig:l1caloscheme}
\end{figure}

The L1Calo consists of three subsystems, namely the preprocessor (PPr), the cluster processor (CP) and the jet/energy-sum  processor (JEP). A schematic representation is shown in Figure~\ref{fig:l1caloscheme}. The preprocessor digitizes the signals coming from the trigger towers, extracts a transverse energy($\et$) value and assigns the signal to a particular LHC bunch crossing. The CP identifies clusters of electrons, photons and hadrons. The JEP identifies jet candidates and calculates the global sum $\et$ as well as missing $\et$ in the event. The information from these modules are passed to the Central Trigger Processor (CTP). Based on the information from L1Calo and L1Muon the CTP sends a Level-1 accept (L1A) signal. When an L1A signal is received, the region of interests as well as the energy sum information are passed to the Level-2 system. In parallel, trigger-tower energies and processing results are handed over to the data acquisition system. This information can be used to monitor the performance of the trigger system. 

\section{Analog Input}
\begin{figure}[h!]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/pulse_Front_30162_FormatApproved.eps}
        \caption{}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/PS_stdDev_hi.eps}
        \caption{}
    \end{subfigure}%
    \caption{ATLAS calorimeter pulse shapes (a) LAr and (b) Tile. Notice that the pulse width spans several bunch crossings}
    \label{fig:pulse_shape}
\end{figure}
The shape of the signals received from the LAr and Tile Calorimeters are different; see Figure~\ref{fig:pulse_shape}. In case of the LAr pulses the rise time is less than \SI{1}{\nano\s} with a linear decay which corresponds to the drift time in the LAr gap. The pulse is amplified at an earlier stage in the front end electronics to make it immune to noise in the subsequent electronics. This pulse is further shaped into a bipolar signal to optimize the signal to noise ratio. The output of the pulse shaper has a rise time of $\sim$\SI{50}{\nano\s} and a long negative undershoot. The positive part of the signal is several bunch cross wide and the area is proportional to the energy deposition in the calorimeter cell. 

The pulses from the TileCal Photomultipliers has a rise time $\sim$\SI{5.5}{\nano\s} and a full width at half maximum of $\sim$\SI{15}{\nano\s}. In order to allow for a common processing for the both calorimeter signals in the trigger system, the tile pulses are shaped into a unipolar pulse with a full width at half maximum equal to $\sim$\SI{50}{\nano\s}, similar to the LAr pulses. 

The signals from multiple cells are summed up in the front end electronics of the two calorimeter systems to form the so called trigger tower. The trigger towers are adjacent and projective with respect to the interaction point. The number of cells in each trigger tower varies from a few in the endcap to 60 in the barrel region. The granularity of the towers in $\Delta\eta\times\Delta\phi = 0.1\times 0.1$ for $|\eta|<2.5$ and coarser at larger pseudorapidities; see Figure~\ref{fig:triggerTowerScheme}.
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{images/chap5/trigTowerGranularity.eps}
    \caption{Trigger tower granularity in $\eta$ and $\phi$ \cite{1748-0221-3-03-P03001}}
    \label{fig:triggerTowerScheme}
\end{figure}

The trigger tower signals are sent from the detector front end to the electronics cavern differentially through 16 way twisted-pair copper cables of various lengths ranging from 30--\SI{70}{\meter}. The receiver system of the L1Calo does fine voltage gain adjustments and signal reordering and sends it to the PreProcessor system (PPr).

The signals that reach the PPr are bipolar signals. These are converted to unipolar signals, the amplitudes are adjusted to conform with a digitization window of \SI{1}{\volt}. These signals are several bunch crossings wide and they are asynchronous in time due to different time of flight of the particles which are responsible for these signals as well as cables accessing different regions of the detector. Before processing begins the signal has to be digitized. The digitization happens at the LHC bunch cross frequency of \SI{40.8}{\mega\hertz}. After digitization these signals are aligned in time before identifying the bunch crossing as well as calculating $\mathrm{E_\text{T}}$.

\section{Hardware}
The description of the hardware follows from~\cite{Andrei:2010zz}. The PPr consists of 124 identical preprocessor modules (PPM). Each of these modules can process 64 trigger tower signals. The PPMs are mounted on 8 crates along with timing units, single board computers as well as optical link transmitter modules. Various units on a PPM is shown in ~\ref{img:ppm_module}

\begin{figure}
    \centering
    \includegraphics[scale=0.5]{images/chap5/PPM_module.eps}
    \caption{The Preprocessor module~\cite{Andrei:2010zz}}
    \label{img:ppm_module}
\end{figure}

The analog input boards condition the signals and send them to 4-channel \textit{Multi-Chip Modules} (PPrMCM), see Figure~\ref{img:mcm} . The PPrMCM stores the event data until a Level-1 Accept signal is received. The \textit{Timing, Trigger and Control Receiver Chip} (TTCrx), decodes the LHC serial protocol signal and places timing information to multiple locations on the PPM. 

\begin{figure}
    \centering
    \includegraphics[scale=0.7]{images/chap5/PPrMCM.eps}
    \caption{PreProcessor Multi-Chip Module: The FADCs convert the analog input signals to digital signals~\cite{Andrei:2010zz}.}
    \label{img:mcm}
\end{figure}

The PPrMCM's consist of four Flash Analog to Digital Converters (FADCs) to digitise signals, one PHOS4 timing chip (PPrPHOS4) to strobe the FADC's, a custom chip PPrASIC to perform trigger specific data processing and LVDS serializers for transmission of critical data to other units in the L1Calo.

The analog signals that are fed to the PPrMCMs are first passed through a low pass filter to reduce high frequency noise components. The signals are then digitized by the FADCs with 10 bit resolution at the LHC bunch-crossing frequency. The \SI{1}{\volt} input voltage range of FADCs correspond to an $\et$ range of 0--\SI{256}{\giga\electronvolt} deposited in a trigger tower. Thus one FADC count corresponds to $\sim$\SI{244}{\mega\electronvolt}. The maximum size of a pulse is 1023 ADC counts. Signals larger than this value are ``saturated signals''. 

The digitization is driven by PPrPHOS4 chips. The phase of the digitization strobe can be fine tuned within steps of \SI{1}{\nano\s} resolution so that the peak of the analog signal can be sampled accurately. The phase of the strobe can also be configured for individual chips to facilitate an accurate sampling.

The resulting four 10 bit FADC data streams are then fed to the PPrASIC where trigger specific data processing is carried out. The real time output of the PPrASIC consists of 3  parallel streams, out of which two provides calibrated $\mathrm{E_{\text{T}}}$ values for the Cluster Processor and the third stream provides  jet $\mathrm{E_\text{T}}$ sums for the Jet Energy Processor.

In the PPrASIC, the FADC signals are first latched with the LHC clock. The latched signals are then delayed by passing through a synchronization First-in-First-Out (FIFO) memory buffer. This is done to align the signal with signals processed in other PPr's. 

Fanned out output of the synchronization buffer is then fed to Bunch Crossing Identification (BCID) algorithms. 

\subsection{BCID Algorithms}

Associating trigger tower signals to the correct LHC bunch crossing is very important for the trigger logic. Since the LAr pulses as well as the tile calorimeter signals are several bunch crosses wide, this is a challenging task. The trigger must also be efficient for low energy particles which produces low amplitude pusles as well as high energy particles which produces saturated pulses.The PPrASIC employs three methods to cover this large dynamic range. One for unsaturated pulses, another for saturated pulses and a third method that provides a cross check for the first two methods. 

The first method uses a finite impulse response (FIR) filter to sharpen the pulse before passing it through a peak finder algorithm. Five consecutive samplings are multiplied with predefined coefficients and summed. The sums of previous, current and the following bunch crossings are compared to find a maximum. The details of other methods can be found in \citep{1748-0221-3-03-P03001}. 


\section{Timing}
A singal in an L1Calo channel trigger tower is an analogue pulse with an amplitude proportional to the energy deposited. The pulse is sampled every bunch crossing.
The shape of the pulse varies slightly depending on the detector region from where it came. The peak of the pulse must be in the center \SI{25}{\nano\second} bunch crossing sample or slice. Five slices is the approximate width of the pulse, which is the minimum required for optimum energy resolution in the calorimeter readout. If the peak is not in the center slice, then the pulse will not have an associated calibration energy value in the correct bunch crossing and hence it will not be summed within the correct event.

It is essential that the system is well timed in order to achieve the best energy resolution for the trigger. Therefore the peak of the pulse should be sampled.

Timing calibration  of a trigger tower so that the peak is in the center slice is known as good ``coarse'' timing. However, to trigger accurately, the peak of the analogue pulse must be in the middel of the center slice. This is known as good ``fine'' timing. The aim is to calibrate and maintain every tower within \SI{2}{\nano\second} of this center. The smallest increment of change on the timing is \SI{1}{\nano\second}

\subsection{Pulse Selection}
Only ``good'' pulses are selected for the analysis~\cite{ellis2012traps}. Fine time is not calculated if the peak 
\begin{itemize}
    \item is less than the peak threshold,
    \item is in the first or last sampled slice,
    \item has adjacent slices equal in size to the peak
    \item has adjacent slices smaller than pedestal value
\end{itemize}

%\begin{figure}[h!]
%    \begin{subfigure}{0.5\textwidth}
%        \includegraphics[width=\textwidth]{./images/chap5/EGammaTauAlgo.eps}
%        \caption{Trigger tower}
%        \label{image:trigger_tower}
%    \end{subfigure}
%    \begin{subfigure}{0.5\textwidth}
%        \includegraphics[width=\textwidth]{./images/chap5/LocalMax.eps}
%        \caption{}
%    \end{subfigure}
%    \caption{}
%\end{figure}


The trigger towers are split into partitions as shown in Table~\ref{table:l1calo_partition}. The trigger towers are coarser than calorimters cells. %Figure~\ref{image:trigger_tower} shows a schematic representation of a trigger tower.

\begin{table}
    \centering
    \begin{tabular}{lll}
        \hline
        Partition name    & Abbreviation & $\eta$ range\\\hline
        \textbf{Electromagnetic layer} & & \\\hline
        Barrel  & EMBA/EMBC & $0<|\eta|<1.4$ \\
        Barrel/End-cap overlap & OverlapA/OverlapC & $1.4<|\eta|<1.5$ \\
        Endcap & EMECA/EMECC & $1.5<|\eta|<3.2$ \\
        Forward Calorimeter &LArFCal1A/LArFCal1C &  $3.2 < |\eta| < 4.9$ \\
        \textbf{Hadronic layer} & & \\\hline
        Long Barrel &TileA/TileC & $0<|\eta|<0.9$ \\
        Extended Barrel & TileA/TileC & $0.9|\eta|<1.5$ \\
        Endcap  & HECA/HECC & $1.5<|\eta|<3.2$\\
        Forward Calorimeter & FCAL23A/FCAL23C & $3.2<|\eta|<4.9$\\\hline
    \end{tabular}
    \caption{L1Calo partitions: Negative $\eta$ side is A side and positive $\eta$ side is C side. }
    \label{table:l1calo_partition}
\end{table}

The signals from the trigger towers are summed using a sliding window algorithm to determine region of interest to identify electrons/photons, tau's, hadrons and jets. This information is sent to the Central Trigger Processor (CTP). The CTP sends an L1 accept signal to the data acquisition based on these informations as well as the information from the L1Muon. 

The signal digitization in the PPr system can be adjusted for individual trigger towers to synchronize the digitization with the LHC clock frequency. A coarse timing in steps of \SI{25}{\nano\s} as well as a fine timing in steps of \SI{1}{\nano\s}, up to \SI{25}{\nano\s} is possible. Both procedures allow to sample the analogue signal peak correctly at its maximum. This is very crucial since incorrect digitization can influence the results of the BCID logic as well as the $E_\text{T}$ sum logic. For a stable and meaningful operation of BCID the sampling of the analogue pulse should be within a precision of $\SI{\pm10}{\nano\second}$.  Similarly a timing precision of $\pm$\SI{5}{\nano\s} is needed for energy measurements with less than 2\% uncertainty. The signal pulses have been  modelled using a Gaus-Landau (GLU) convolution or double Landau function (LLU) for various detector regions and have been calibrated by an earlier effort~\cite{Lang:1454684}. Results obtained from the GLU fit for typical L1Calo pulse are shown in Figure~\ref{fig:gluFit}

There are several factors which may contribute to change in timing. This includes random changes due to electronics, signal transmission problems and problems in the hardware. It has been found that timing shifts occur during the operation of the detector. For instance the thermal expansion of the signal cables due to weather changes causes shifts in timing. Potential electronic noise could also cause deviations from sampling the pulses correctly. Hence the timing behavior of the sampling strobe has to be monitored. The GLU, LLU functions have six free parameters for the fit and it would be computationally expensive for the data acquisition system to do fits for $\sim$7200 L1Calo channels during every bunch crossing. Therefore a simple alternative wad deviced. Instead of fitting, the peak region the pulse described by the central three ADC slices (Figure~\ref{fig:adcScheme}) is approximated using a second order polynomial. Solving for the maxima, the peak position is determined.

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{images/chap5/adcsampling.eps}
        \caption{}
        \label{fig:adcScheme}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{./images/chap5/ATL-DAQ-PUB-2010-001-fig_01a.eps}
        \caption{}
        \label{fig:gluFit}
    \end{subfigure}
    \caption{ (a) Schematic representation of signal sampling: The blue line indicates the central slice. Using these three slices and assuming they are described by a parabola the peak position is found. (b) A hybrid Gaus Landau (GLU) function is fit to the signals read out from the L1Calo system}
\end{figure}

We model the signal peak region using a second order polynomial 
\begin{align}
f(x)      &=      ax^2 + bx + c 
\end{align}

This function has a maximum at 
\begin{align}
f_\text{max} = \frac{-b}{2a} 
\end{align}

Since the analog signal is sampled at \SI{40.8}{\mega\hertz} the time interval between successive samples is \SI{25}{\nano\s}. Therefore in our model, the function assumes only discrete values.  
Adding some boundary conditions namely
\begin{align}
    f(0)    &= c \\
    f(-25)  &= 25^2a-25b +c\\
    f(25)   &= 25^2a+25b +c
\end{align}
solving for $a$ and $b$
\begin{align}
    a &= \frac{f(-25) + f(25) - 2f(0)}{2\times25^2}\\
    b &= \frac{f(25) - f(-25)}{2\times25}
\end{align}

the expression for fine time is obtained.
\begin{align}
    \mathbf{FineTime}&\equiv f_\text{max} = \frac{f(-25) - f(25)}{2[f(-25) + f(25) - 2f(0)]}\times 25 \text{ ns}
    \label{eqn:fineTime}
\end{align}

%Eq.~\ref{eqn:fineTime} can be compared with Figure~\ref{fig:adcScheme}. The values of $f(x^\prime$) can be replaced with respective sampling slices to get the value of \textit{FineTime}

\begin{figure}
    \centering
    %\caption{The average and rms of individual trigger tower finetime. Data from run 191426 (22 october 2011) compared with beam phase as measured by the Central Trigger}
 
    %\begin{subfigure}{.5\textwidth}
    %    \centering
    %    \includegraphics[width=\textwidth]{./images/chap5/finetime_beamphase_em.eps}
    %    \caption{Electromagnetic layer}
    %\end{subfigure}%
    %\begin{subfigure}{.5\textwidth}
    %    \centering
    %    \includegraphics[width=\textwidth]{./images/chap5/finetime_beamphase_had.eps}
    %    \caption{Electromagnetic layer}
    %\end{subfigure}%

    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/finetime_lumi_em.eps}
        \caption{Electromagnetic layer}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/finetime_lumi_had.eps}
        \caption{Hadronic layer}
    \end{subfigure}%

    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/finetimeEMB_lumi.eps}
        \caption{Electromagnetic barrel region}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/finetimeTileB_lumi.eps}
        \caption{Tile barrel region}
    \end{subfigure}%

    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/etaVsPhiEm.eps}
        \caption{Electromagnetic trigger tower}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./images/chap5/etaVsPhiHad.eps}
        \caption{Hadronic trigger tower}
    \end{subfigure}
    \caption{Average trigger tower fine time. The histograms on the top show average and rms of fine time for  the electromagnetic as well as the hadronic layers of the trigger tower. The middle histograms shows average and rms fine time for the barrel region of the electromagnetic as well as the hadronic layers of the trigger tower. Finally average fine time is shown for all the L1Calo channels as an $\eta-\phi$ map. The fluctuations are within the tolerable range of an uncertainty of $\pm5\text{ ns}$ }
    \label{fig:results}
\end{figure}

A software package has been written to calculate \textit{FineTime} for all the 7168 L1Calo channels individually. The \textit{FineTime} calculated are filled in histograms specific to individual trigger tower channels as well as different L1Calo partitions. Any anomalies or sudden shifts in \textit{FineTime} is an indication of discrepancy in sampling the peak of the analog pulse, which eventually compromises the performance of the L1Calo system and thus valuable physics information. The monitoring histograms are available as live histograms at the desk of the ATLAS trigger shifter in the ATLAS control room. In case of an abnormal behavior the shifter can take necessary actions to bring back the system to normalcy or notify the expert concerned. The package also makes \textit{FineTime} histograms available for a detailed analysis later, once the data is stored on disk. Selected sample histograms are shown in Figure~\ref{fig:results}
\section{Conclusion}
A software framework for monitoring fine time shifts has been developed. The software is part of the ATLAS analysis package \textit{Athena}. Monitoring histograms are produced during live data taking. The package also offers the possibility to make FineTime histograms for offline data. The monitoring histograms show that there has been no drastic fine time shifts for the data taking periods. 


%\bibliographystyle{atlasBibStyleWithTitle}
%\bibliography{chap5_l1calo} % - - > Name of your .bib file
